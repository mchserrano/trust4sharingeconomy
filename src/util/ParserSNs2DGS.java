package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;

import org.graphstream.algorithm.Toolkit;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSinkDGS;
import org.graphstream.ui.view.Viewer;
import org.apache.commons.cli.*;   // for CLI parsing arguments

//----------------------------- MAIN FUNCTION -----------------------------//

/**
 * Main function to read SNs from text file (two columns for edges) and store into a DGS file (GraphStream).
 * 
 * @param args
 */

public class ParserSNs2DGS {		
	
	// PRIVATE METHODS
		
		
	/** 
	 * read the text file and create a DGS graph from them
	 * no strict and auto create is ON
	 *
	 * @param file
	 * @param tSetGen
	 * @throws IOException
	 * @throws ParseException
	 */
	private static void readRelationsshipsAndCreateFile (
			String file, String name
			) throws IOException, NumberFormatException  {

		String thisLine = null;	   

		BufferedReader in = new BufferedReader(new FileReader(file));

		Graph graph = new SingleGraph(name);
		
		// we just create nodes from the relationships
		graph.setStrict(false);
    	graph.setAutoCreate(true);
		
		// read first line (header)
		thisLine = in.readLine();				
		
	    while ((thisLine = in.readLine()) != null) {
	    	
	    	// split the line for two edges
	    	String[] nodes = thisLine.split(" ");	    
	    		    	
	    	graph.addEdge( nodes[0] + "--" + nodes[1], nodes[0], nodes[1] );

	    	//System.out.println("Edge between " + nodes[0] + " and " + nodes[1] + ", added");
	    }       
	    
		in.close();	 
		
		// save to a file
		FileSinkDGS fileSink = new FileSinkDGS();
		fileSink.writeAll(graph,  name );
		
		// some stats
		
		System.out.println("Density: "+ Toolkit.density(graph));
		System.out.println("Avg. degree: " + Toolkit.averageDegree(graph));
		//System.out.println("Diameter: " + Toolkit.diameter(graph));
		System.out.println("CC: " + Toolkit.averageClusteringCoefficient(graph));
		
		System.out.println("No. of nodes: " + graph.getNodeCount() 
				+ "\nNo. of edges: " + graph.getEdgeSet().size() );
		
		int distr[] = Toolkit.degreeDistribution(graph);
		System.out.println("\nDistribution degree: "); 
		for (int k = 0; k < distr.length; k++)
			System.out.println("degree "  + k + "; " + distr[k]);
		
		// to show the SN
		
		// Populate the graph.
		Viewer viewer = graph.display();
		
		// Let the layout work ...
		viewer.disableAutoLayout();
		// Do some work ...
		viewer.enableAutoLayout();
	
	}
	
	
	/** 
	 *   MAIN FUNCTION
	 */
	public static void main (String[] args) {

		String textFileName = "";
		String outputFileName = "";
	    
	    try {
	    	
	    	// parsing the options
			Options options = new Options();
			
			options.addOption("textFile", true, "Text file with the edges between nodes");
			options.getOption("textFile").setRequired(true);
			options.getOption("textFile").setOptionalArg(false);

			options.addOption("outputDGSName", true, "DGS file for the converted network");
			options.getOption("outputDGSName").setRequired(true);
			
			// create the parser
		    CommandLineParser parser = new DefaultParser();
		    
	        // parse the command line arguments for the given options
	        CommandLine line = parser.parse (options, args);
	        
	        // retrieve the arguments
	        		    
		    if( line.hasOption( "textFile" ) )		    
		    	textFileName = line.getOptionValue("textFile");
		    
			if( line.hasOption( "outputDGSName" ) )			    
		    	outputFileName = line.getOptionValue("outputDGSName");		    	  	
			else 
				outputFileName = String.valueOf("PARSED_SN");
				
		    // help information
		    if( line.hasOption("help") ) {
			    	
			    // automatically generate the help statement
			    HelpFormatter formatter = new HelpFormatter();
			    formatter.printHelp( "ParserSNs2DGS. 4th July 2016. Manuel Chica. UOC", options );			   	
			}

			// call the parsing function
		    
		    readRelationsshipsAndCreateFile (textFileName, outputFileName);

			
			System.out.println("---------------\nParsing ended successfully from text file " + 
					textFileName + " to DGS file " + outputFileName);
			
	    } catch( org.apache.commons.cli.ParseException exp ) {
	    	
	        // oops, something went wrong
	        System.err.println( "Parsing failed. Reason: " + exp.getMessage() );
	        
	    } catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
	    		

	}
	
}
