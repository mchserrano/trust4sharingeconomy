package controller;

import view.RunStats;

import java.util.logging.Level;
import java.util.logging.Logger;

import model.*;

/** 
 * Controller
 * 
 * This class is the controller to call the model
 * It calls the model, run it to get all the steps and returns a list
 * of simulated values
 * 
 * @author mchica
 *
 */

public class Controller {

	// LOGGING
	private static final Logger log = Logger.getLogger( Model.class.getName() );
	
	protected Model model = null;
	
	/**
	 * Clone the object
	 */
    /*public Object clone() {
    	Controller m = (Controller)(this.clone());
        m.model = (Model)(model.clone());
        
        return m;        
    }*/
    
	/**
	 * @return the ModelParameters object where all the parameters are defined
	 */
	public ModelParameters getModelParameters() {
		return model.getParametersObject();
	}


	/**
	 * Set the ModelParameters object for all the defined parameters 
	 */
	public void setModelParameters(ModelParameters _params) {
		model.setParametersObject(_params);
	}

	
	/**
	 * @return the maxSteps
	 */
	/*public long getMaxSteps() {
		return maxSteps;
	}*/

	/**
	 * sets the maxSteps
	 */
	/*public void setMaxSteps(long maxSteps) {
		this.maxSteps = maxSteps;
	}*/
	
		
	/**
	 * Constructor having config, seed and maxSteps
	 */
	public Controller(ModelParameters _params, String _paramsFile) {
		
		
		Model.setConfigFileName(_paramsFile);
		
		//PropertyConfigurator.configure(Model.getLogFileName());

		this.model = new Model(_params);
		      
	}
		
	
	/**
	 * Run the model one time
	 * @return the statistics after running the model
	 * 
	 */
	public RunStats runModel() {
		
		// starting and looping the mode
		
		//try {
		    
			// object to store results into the stats object
			
			RunStats stats = new RunStats(model.getParametersObject().getRunsMC(),
					model.getParametersObject().getMaxSteps());

			log.log(Level.FINE, "\n**Starting MC simulation with:\n");
			log.log (Level.FINE, "\n" + model.getParametersObject().export()  + "\n");	

	        System.out.print("\nMC runs (" + model.getParametersObject().getRunsMC() + "): ");
	        log.log(Level.FINE, "\nMC runs (" + model.getParametersObject().getRunsMC() + "): ");
			
			for (int i = 0; i <  model.getParametersObject().getRunsMC(); i++) {

				// for each MC run we start the model and run it
        		        
				model.start();
				
				do {
					
					if (!model.schedule.step(model)) break;
					 				
				} while (model.schedule.getSteps() < model.getParametersObject().getMaxSteps());			

				model.finish();	
						        
		        // store to the stats object
		        
				stats.setTPForRun(i, model.getTP_AgentsArray());
				stats.setUPForRun(i, model.getUP_AgentsArray());
				stats.setTCForRun(i, model.getTC_AgentsArray());
				stats.setUCForRun(i, model.getUC_AgentsArray());
				
				stats.setStrategyChangesForRun(i, model.getStrategyChanges_AgentsArray());
				
				// TODO doing both, saving the global wealth in the final step and in the last n steps (e.g., 25%)
				stats.setnetWealthForRun(i, model.getGlobalPayoffs());
				
				// to show some logs about activity of the agents
				// model.printStatisticsScreen ();				
				
				log.log(Level.FINE, ".");
		        System.out.print(".");
		      
			}						

			log.log(Level.FINE, " -> Ended!");
	        System.out.println(" -> Ended!");
	        
			return stats;
			
			/*} catch (Exception e) {
			
				System.err.println("Controller: Error when running model, execution is aborted.\n"
							+ e.getMessage());
			}*/
		
		//return null;
	}
		
}
