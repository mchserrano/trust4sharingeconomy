package view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


import controller.Controller;
import model.Model;
import model.ModelParameters;


/**
 * SA class to run and save results when running the MC model 
 * with different parameter configuration from a config base
 * 
 * @param args
 */

public class SensitivityAnalysis {		
		
	
	// LOGGING
	private static final Logger log = Logger.getLogger( Model.class.getName() );
	
		
	/**
	 * Static function to run a SA for the R parameter
	 * 
	 */	
	public static void runSA_R (ModelParameters _params, String _paramsFile, 
			File fileAllMC, File fileSummaryMCRuns, File fileAllMCLQ, File fileSummaryMCRunsLQ) {

		// create output files in case they exist as we will append the simulation contents
		

		if (fileAllMC.exists() && !fileAllMC.isDirectory()) {
			fileAllMC.delete();
		}
		
		if (fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory()) {
			fileSummaryMCRuns.delete();
		}
		
		if (fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory()) {
			fileAllMCLQ.delete();
		}
		
		if (fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory()) {
			fileSummaryMCRunsLQ.delete();
		}
		
		// *********************  Array with the ratio values 
		// R is between Temp and S as Temp > R > S > X 
		// therefore we will create all the values between Temp and S 
		// (both inclusive even if they are not feasible) with a given step
		
		// create the arrays with the R values
		ArrayList<Double> valuesRatio = new ArrayList<Double>();
		
		// save the values to the array of ratios
		double v = _params.getTemp() - 1;
		double step = 2;
		int k = 0;
		while (v >= (_params.getS() + 1)){	
				    	
			valuesRatio.add(k, v);
			v = v - step;
			k ++;
		}

	    Controller controller = new Controller (_params, _paramsFile);

	    for (int i = 0; i < valuesRatio.size(); i++ ) {
		    		
    		// set the given R value to the params (the only changing parameter)  	
	    	double newR = valuesRatio.get(i);
	    	
	    	_params.setR(newR);

	        System.out.println("-> OAT for parameter R of " + _params.getR() + 
	        		" (with fixed Temp = " + _params.getTemp() + ", S = " + _params.getS() + ", X = " + _params.getX() + ")");

			log.log(Level.FINE, "\n****** Running Monte-Carlo simulation for OAT: parameter R of " + _params.getR() + 
	        		" (with fixed Temp = " + _params.getTemp() + ", S = " + _params.getS() + ", X = " + _params.getX() + ")"
					+ " \n");
		
			RunStats stats;

			long time1 = System.currentTimeMillis ();
			
	 		// running the model with the MC simulations
	 		stats = controller.runModel();		

	 		long  time2  = System.currentTimeMillis( );
	 		System.out.println("\n****** " + (double)(time2 - time1)/1000 + "s spent during the simulation");

	 		// calculate the stats for this run and set the name of this experiment
	 		stats.setExpName("RValue;" + _params.getR() + ";TempValue;" + _params.getTemp()  + ";SValue;" + _params.getS()  + ";XValue;" + _params.getX());		    	
	 		stats.calcAllStats();

			PrintWriter out = new PrintWriter(System.out, true);
			_params.printParameters(out);
			 
	 		// print the stats in the screen 		
	        System.out.println("\n****** Stats of R parameter OAT configuration ******\n");
	 		stats.printSummaryStats(out, false);
	 		stats.printSummaryStatsByAveragingLastQuartile(out, false);

			// print the stats into a file
			System.out.println("\n****** Stats of R parameter OAT configuration also saved into a file ******\n");
			
			PrintWriter printWriter;
	         
	 		try {
	 			
	 			// print all the runs in a file 			
	 			printWriter = new PrintWriter (new FileOutputStream(fileAllMC, true));
	 			
	 			if ( fileAllMC.exists() && !fileAllMC.isDirectory() )
	 		        stats.appendAllStats(printWriter);		 		        
	 		    else
		 			stats.printAllStats(printWriter, false);		 		  
	 		    
	 	        printWriter.close ();       	
	 	        
	 	        // print the summarized MC runs in a file
	 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRuns, true));
	 	        
	 	       if ( fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory() )
	 	    	    stats.appendSummaryStats(printWriter);		 		        
	 		    else		 		    	
	 		    	stats.printSummaryStats(printWriter, false);
	 		    		 				 			
	 	        printWriter.close (); 
	 	        
	 	        // print all the runs in a file 			
	 			printWriter = new PrintWriter (new FileOutputStream(fileAllMCLQ, true));
	 			
	 			if ( fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory() )
		 			stats.printAllStatsByAveragingLastQuartile(printWriter, true);		 		 
	 		    else
		 			stats.printAllStatsByAveragingLastQuartile(printWriter, false);		 		  
	 		    
	 	        printWriter.close ();       	
	 	        
	 	        // print the summarized MC runs in a file
	 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRunsLQ, true));
	 	        
	 	       if ( fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory() )
	 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, true);
	 		    else		 		    	
	 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, false);
	 		    		 				 			
	 	        printWriter.close ();  
	 	        
	 		} catch (FileNotFoundException e) {
	 			
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 			
	 		    log.log( Level.SEVERE, e.toString(), e );
	 		} 
    	}
    	
    }				
		
	/**
	 * Static function to run a SA for the R and X parameters
	 * 
	 */	
	public static void runSA_R_X (ModelParameters _params, String _paramsFile, 
			File fileAllMC, File fileSummaryMCRuns, File fileAllMCLQ, File fileSummaryMCRunsLQ) {

		// create output files in case they exist as we will append the simulation contents
		

		if (fileAllMC.exists() && !fileAllMC.isDirectory()) {
			fileAllMC.delete();
		}
		
		if (fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory()) {
			fileSummaryMCRuns.delete();
		}
		
		if (fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory()) {
			fileAllMCLQ.delete();
		}
		
		if (fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory()) {
			fileSummaryMCRunsLQ.delete();
		}
		
		// *********************  Array with the ratio values 
		// R is between Temp and S as Temp > R > S > X 
		// therefore we will create all the values between Temp and S 
		// (both inclusive even if they are not feasible) with a given step
		
		// X is between 0 and S 
		// therefore we will create all its possible values
		// (both inclusive even if they are not feasible) with a given step
							
		// the given step for both parameters X and R
		double step = 1;
		
		// create the arrays with the R values
		ArrayList<Double> valuesRatio = new ArrayList<Double>();
		
		// save the values to the array of Rs starting from Temp
		// in order to save time with the SA we will start R in 32 instead of Temp value
		double v = 30;  
		
		int k = 0;
		while (v >= (_params.getS())){	 // until R is equal to S
				    	
			valuesRatio.add(k, v);
			v = v - step;
			k ++;
		}

		// create the arrays with the X values
		ArrayList<Double> valuesX = new ArrayList<Double>();
		
		// save the values to the array of X values starting from S
		v = _params.getS();
		
		k = 0;
		while (v >= 0){	 // until X is equal to 0	    	
				    	
			valuesX.add(k, v);
			v = v - step;
			k ++;
		}
				
	    Controller controller = new Controller (_params, _paramsFile);

	    for (int j = 0; j < valuesX.size(); j++ ) {
	    		    
		    for (int i = 0; i < valuesRatio.size(); i++ ) {
			    		
	    		// set the given R value to the params (one of the two changing parameters)  	
		    	double newR = valuesRatio.get(i);		    	
		    	_params.setR(newR);
		    	
	    		// set the given X value to the params (one of the two changing parameters)   	
		    	double newX = valuesX.get(j);	    	
		    	_params.setX(newX);
	
		        System.out.println("-> OAT for parameters R of " + _params.getR() + " and X of " + _params.getX() +
		        		" (with fixed Temp = " + _params.getTemp() + " and S = " + _params.getS()  + ")");
	
				log.log(Level.FINE, "\n****** Running Monte-Carlo simulation for OAT: parameters R of " + _params.getR() + 
						" and X of " + _params.getX() +	" (with fixed Temp = " + _params.getTemp() + ", S = " + _params.getS() + ")"
						+ " \n");
			
				RunStats stats;
	
				long time1 = System.currentTimeMillis ();
				
		 		// running the model with the MC simulations
		 		stats = controller.runModel();		
	
		 		long  time2  = System.currentTimeMillis( );
		 		System.out.println("\n****** " + (double)(time2 - time1)/1000 + "s spent during the simulation");
	
		 		// calculate the stats for this run and set the name of this experiment
		 		stats.setExpName("RValue;" + _params.getR() + ";TempValue;" + _params.getTemp()  + ";SValue;" + _params.getS()  + ";XValue;" + _params.getX());		    	
		 		stats.calcAllStats();
	
				PrintWriter out = new PrintWriter(System.out, true);
				_params.printParameters(out);
				 
		 		// print the stats in the screen 		
		        System.out.println("\n****** Stats of R/X parameters OAT configuration ******\n");
		 		stats.printSummaryStats(out, false);
		 		stats.printSummaryStatsByAveragingLastQuartile(out, false);
	
				// print the stats into a file
				System.out.println("\n****** Stats of R/X parameters OAT configuration also saved into a file ******\n");
				
				PrintWriter printWriter;
		         
		 		try {
		 			
		 			// print all the runs in a file 			
		 			printWriter = new PrintWriter (new FileOutputStream(fileAllMC, true));
		 			
		 			if ( fileAllMC.exists() && !fileAllMC.isDirectory() )
		 		        stats.appendAllStats(printWriter);		 		        
		 		    else
			 			stats.printAllStats(printWriter, false);		 		  
		 		    
		 	        printWriter.close ();       	
		 	        
		 	        // print the summarized MC runs in a file
		 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRuns, true));
		 	        
		 	       if ( fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory() )
		 	    	    stats.appendSummaryStats(printWriter);		 		        
		 		    else		 		    	
		 		    	stats.printSummaryStats(printWriter, false);
		 		    		 				 			
		 	        printWriter.close (); 
		 	        
		 	        // print all the runs in a file 			
		 			printWriter = new PrintWriter (new FileOutputStream(fileAllMCLQ, true));
		 			
		 			if ( fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory() )
			 			stats.printAllStatsByAveragingLastQuartile(printWriter, true);		 		 
		 		    else
			 			stats.printAllStatsByAveragingLastQuartile(printWriter, false);		 		  
		 		    
		 	        printWriter.close ();       	
		 	        
		 	        // print the summarized MC runs in a file
		 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRunsLQ, true));
		 	        
		 	       if ( fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory() )
		 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, true);
		 		    else		 		    	
		 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, false);
		 		    		 				 			
		 	        printWriter.close ();  
		 	        
		 		} catch (FileNotFoundException e) {
		 			
		 			// TODO Auto-generated catch block
		 			e.printStackTrace();
		 			
		 		    log.log( Level.SEVERE, e.toString(), e );
		 		} 
	    	}
	    }
    	
    }			
}
