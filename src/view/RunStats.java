package view;

// package for std and mean

import java.io.PrintWriter;

import org.apache.commons.math3.stat.descriptive.*;


/** 
 * StatsRun for the Sharing Economy Trust Model
 * 
 * This class will store all the results for the MonteCarlo simulation.
 * It will also update the stats and error metrics w.r.t. the historical data.
 * 
 * @date 2016/12/21
 * @author mchica
 *
 */
public class RunStats {

	private int numberRuns;				// number of MC simulations

	private int numberSteps;			// number of steps simulation
	
	// fields for TP members of the population (time series)
	private int TP[][];			// all the TP members (each for run and step)
	private double avgTP[];			// average TP over all the runs for each step
	private double stdTP[];			// std TP over all the runs for each step
	private double[] min_TP;
	private double[] max_TP;
	
	// fields for UP members of the population (time series)
	private int UP[][];			// all the UP members (each for run and step)
	private double avgUP[];			// average UP over all the runs for each step
	private double stdUP[];			// std UP over all the runs for each step
	private double[] min_UP;
	private double[] max_UP;
	
	// fields for TC members of the population (time series)
	private int TC[][];			// all the TC members (each for run and step)
	private double avgTC[];			// average TC over all the runs for each step
	private double stdTC[];			// std TC over all the runs for each step
	private double[] min_TC;
	private double[] max_TC;
	
	// fields for UC members of the population (time series)
	private int UC[][];			// all the UC members (each for run and step)
	private double avgUC[];			// average UC over all the runs for each step
	private double stdUC[];			// std UC over all the runs for each step
	private double[] min_UC;
	private double[] max_UC;
	
	// fields for netWealth
	private double netWealth[][];			// all the netWealth metrics (each for run and step)
	private double avgnetWealth[];			// average over all the runs for each step
	private double stdnetWealth[];			// std over all the runs for each step
	private double[] min_netWealth;
	private double[] max_netWealth;
	
	// fields for changes in the agents' strategies
	private int strategyChanges[][];			// the number of strategy changes in the agentes (each for run and step)
	private double avgStrategyChanges[];			// average of the number of strategy changes over all the runs for each step
	private double stdStrategyChanges[];			// std of the number of strategy changes over all the runs for each step
	private double[] min_StrategyChanges;		// min of the number of strategy changes over all the runs for each step
	private double[] max_StrategyChanges;		// max of the number of strategy changes over all the runs for each step
				
	private String expName;				// the key name of this experiment (all the MC runs)
	
	
	// ########################################################################	
	// Methods/Functions 	
	// ########################################################################

	//--------------------------- Getters and setters ---------------------------//


	/**
	 * @return the numberRuns
	 */
	public int getNumberRuns() {
		return numberRuns;
	}

	/**
	 * @param numberRuns the numberRuns to set
	 */
	public void setNumberRuns(int _numberRuns) {
		this.numberRuns = _numberRuns;
	}

	/**
	 * @return the stdUP
	 */
	public double[] getStdUP() {
		return stdUP;
	}

	/**
	 * @param stdUP the stdUP to set
	 */
	public void setStdUP(double _stdUP[]) {
		this.stdUP= _stdUP;
	}

	/**
	 * @return the avgUP
	 */
	public double[] getAvgUP() {
		return avgUP;
	}

	/**
	 * @param avgChebyshev the avgUP to set
	 */
	public void setAvgUP(double _avgUP[]) {
		this.avgUP = _avgUP;
	}

	/**
	 * @return the UP
	 */
	public int[][] getUP() {
		return UP;
	}

	/**
	 * @param chebyshev the UP to set
	 */
	public void setUP(int _UP[][]) {
		this.UP = _UP;
	}

	/**
	 * @return the stdTC
	 */
	public double[] getStdStrategyChanges() {
		return stdStrategyChanges;
	}

	/**
	 * @param stdStrategyChanges the stdStrategyChanges to set
	 */
	public void setStdStrategyChanges(double[] _stdStrategyChanges) {
		this.stdStrategyChanges = _stdStrategyChanges;
	}

	/**
	 * @return the avgStrategyChanges
	 */
	public double[] getAvgStrategyChanges() {
		return avgStrategyChanges;
	}

	/**
	 * @param the avgStrategyChanges to set
	 */
	public void setAvgStrategyChanges(double _avgStrategyChanges[]) {
		this.avgStrategyChanges = _avgStrategyChanges;
	}

	/**
	 * @return the strategyChanges
	 */
	public int[][] getStrategyChanges() {
		return strategyChanges;
	}

	/**
	 * @param _StrategyChanges the StrategyChanges to set
	 */
	public void setStrategyChanges(int _StrategyChanges[][]) {
		this.strategyChanges = _StrategyChanges;
	}
	
	/**
	 * @return the stdTC
	 */
	public double[] getStdTC() {
		return stdTC;
	}

	/**
	 * @param stdTC the stdTC to set
	 */
	public void setStdTC(double[] _stdTC) {
		this.stdTC = _stdTC;
	}

	/**
	 * @return the avgTC
	 */
	public double[] getAvgTC() {
		return avgTC;
	}

	/**
	 * @param avgTC the avgTC to set
	 */
	public void setAvgTC(double _avgTC[]) {
		this.avgTC = _avgTC;
	}

	/**
	 * @return the TC
	 */
	public int[][] getTC() {
		return TC;
	}

	/**
	 * @param _TC the TC to set
	 */
	public void setTC(int _TC[][]) {
		this.TC = _TC;
	}

	/**
	 * @return the stdnetWealth
	 */
	public double[] getStdnetWealth() {
		return stdnetWealth;
	}

	/**
	 * @param stdnetWealth the stdnetWealth to set
	 */
	public void setStdnetWealth(double[] _stdnetWealth) {
		this.stdnetWealth = _stdnetWealth;
	}

	/**
	 * @return the avgnetWealth
	 */
	public double[] getAvgnetWealth() {
		return avgnetWealth;
	}
	
	public String getExpName() {
		return expName;
	}

	public void setExpName(String expName) {
		this.expName = expName;
	}

	/**
	 * @param avgnetWealth the avgnetWealth to set
	 */
	public void setAvgnetWealth(double[] _avgnetWealth) {
		this.avgnetWealth = _avgnetWealth;
	}

	/**
	 * @return the netWealth
	 */
	public double[][] getnetWealth() {
		return netWealth;
	}

	/**
	 * @param netWealth the netWealth to set for run _numberOfRun
	 */
	public void setnetWealthForRun(int _numberOfRun, double _netWealth[]) {
		
		for (int i = 0; i < this.numberSteps; i++)
			this.netWealth[_numberOfRun][i] = _netWealth[i];
	}
	
	/**
	 * @param _TP TP value to set for run _numberOfRun
	 */
	public void setTPForRun(int _numberOfRun, int _TP[]) {
		
		for (int i = 0; i < this.numberSteps; i++)
			this.TP[_numberOfRun][i] = _TP[i];
		
	}

	/**
	 * @param _UP UP value to set for run _numberOfRun
	 */
	public void setUPForRun(int _numberOfRun, int _UP[]) {
		
		for (int i = 0; i < this.numberSteps; i++)
			this.UP[_numberOfRun][i] = _UP[i];
		
	}

	/**
	 * @param _TC TC value to set for run _numberOfRun
	 */
	public void setTCForRun(int _numberOfRun, int _TC[]) {
		
		for (int i = 0; i < this.numberSteps; i++)
			this.TC[_numberOfRun][i] = _TC[i];
		
	}
	
	/**
	 * @param _UC UC value to set for run _numberOfRun
	 */
	public void setUCForRun(int _numberOfRun, int _UC[]) {
		
		for (int i = 0; i < this.numberSteps; i++)
			this.UC[_numberOfRun][i] = _UC[i];
		
	}
	
	/**
	 * @param strategyChanges value to set for run _numberOfRun
	 */
	public void setStrategyChangesForRun(int _numberOfRun, int _strategyChanges[]) {
		
		for (int i = 0; i < this.numberSteps; i++)
			this.strategyChanges[_numberOfRun][i] = _strategyChanges[i];
		
	}
		
	/**
	 * @param netWealth the netWealth to set
	 */
	public void setnetWealth(double _netWealth[][]) {
		
		this.netWealth = _netWealth;
	}
	
	/**
	 * @return the stdTP
	 */
	public double[] getStdTP() {
		return stdTP;
	}

	/**
	 * @param stdTP the stdTP to set
	 */
	public void setStdTP(double[] _stdTP) {
		this.stdTP = _stdTP;
	}

	/**
	 * @return the avgTP
	 */
	public double[] getAvgTP() {
		return avgTP;
	}

	/**
	 * @param avgTP the avgTP to set
	 */
	public void setAvgTP(double _avgTP[]) {
		this.avgTP = _avgTP;
	}

	/**
	 * @return the TP
	 */
	public int[][] getTP() {
		return TP;
	}

	/**
	 * @param TP the TP to set
	 */
	public void setTP(int _TP[][]) {
		this.TP = _TP;
	}
	
	/**
	 * @return the numberSteps
	 */
	public int getNumberSteps() {
		return numberSteps;
	}

	/**
	 * @param numberSteps the numberSteps to set
	 */
	public void setNumberSteps(int _numberSteps) {
		this.numberSteps = _numberSteps;
	}
	
    
	//--------------------------- Constructor ---------------------------//
	/**
	 * constructor of Stats
	 * @param _nRuns
	 */
	public RunStats (int _nRuns, int _nSteps){

		numberRuns = _nRuns;
		numberSteps = _nSteps;
		
		// allocating space for metrics
		this.TP = new int[_nRuns][_nSteps];
		this.UP = new int[_nRuns][_nSteps];
		this.TC = new int[_nRuns][_nSteps];
		this.UC = new int[_nRuns][_nSteps];
		this.netWealth = new double[_nRuns][_nSteps];
		this.strategyChanges = new int[_nRuns][_nSteps];

		this.avgTP = new double[_nSteps];	
		this.stdTP = new double[_nSteps];
		this.min_TP = new double[_nSteps];	
		this.max_TP = new double[_nSteps];

		this.avgUP = new double[_nSteps];	
		this.stdUP = new double[_nSteps];
		this.min_UP = new double[_nSteps];	
		this.max_UP = new double[_nSteps];

		this.avgTC = new double[_nSteps];	
		this.stdTC = new double[_nSteps];
		this.min_TC = new double[_nSteps];	
		this.max_TC = new double[_nSteps];

		this.avgUC = new double[_nSteps];	
		this.stdUC = new double[_nSteps];
		this.min_UC = new double[_nSteps];	
		this.max_UC = new double[_nSteps];
		
		this.avgnetWealth = new double[_nSteps];	
		this.stdnetWealth = new double[_nSteps];
		this.min_netWealth = new double[_nSteps];	
		this.max_netWealth = new double[_nSteps];

		this.avgStrategyChanges = new double[_nSteps];	
		this.stdStrategyChanges = new double[_nSteps];
		this.min_StrategyChanges = new double[_nSteps];	
		this.max_StrategyChanges = new double[_nSteps];
		
	}
		
	/**
	 * This method prints all the steps values (avg and stdev of the MC RUNS) to a stream file 
	 * (or to the console)
	 * @param writer that is opened before calling the function
	 */
	public void printTimeSeriesStats(PrintWriter writer) {
		
		writer.println("step;netWealthAvg;netWealthStd;netWealthMin;netWealthMax;"
				+ "TPAvg;TPStd;TPMin;TPMax;UPAvg;UPStd;UPMin;UPMax;TCAvg;TCStd;TCMin;TCMax;UCAvg;UCStd;UCMin;UCMax;" +
				"strategyChangesAvg;strategyChangesStd;strategyChangesMin;strategyChangesMax;");
		
		for (int i = 0; i < this.numberSteps; i++) {
			
			String toPrint = i + ";" 
					+ String.format("%.4f", this.avgnetWealth[i]) + ";" + String.format("%.4f", this.stdnetWealth[i]) + ";" 
					+ String.format("%.4f", this.min_netWealth[i]) + ";" + String.format("%.4f", this.max_netWealth[i]) + ";"
					+ String.format("%.4f", this.avgTP[i]) + ";" + String.format("%.4f", this.stdTP[i]) + ";" 
					+ String.format("%.4f", this.min_TP[i]) + ";" + String.format("%.4f", this.max_TP[i]) + ";"
					+ String.format("%.4f", this.avgUP[i]) + ";" + String.format("%.4f", this.stdUP[i]) + ";" 
					+ String.format("%.4f", this.min_UP[i]) + ";" + String.format("%.4f", this.max_UP[i]) + ";"
					+ String.format("%.4f", this.avgTC[i]) + ";" + String.format("%.4f", this.stdTC[i]) + ";" 
					+ String.format("%.4f", this.min_TC[i]) + ";"+ String.format("%.4f", this.max_TC[i]) + ";"
					+ String.format("%.4f", this.avgUC[i]) + ";" + String.format("%.4f", this.stdUC[i]) + ";" 
					+ String.format("%.4f", this.min_UC[i]) + ";"+ String.format("%.4f", this.max_UC[i]) + ";"
					+ String.format("%.4f", this.avgStrategyChanges[i]) + ";" + String.format("%.4f", this.stdStrategyChanges[i]) + ";" 
					+ String.format("%.4f", this.min_StrategyChanges[i]) + ";" + String.format("%.4f", this.max_StrategyChanges[i]) + ";\n";
					
			writer.print (toPrint);
		}
			
	}
		

	/**
	 * This method prints summarized stats (avg and std of MC runs) to a stream file (or to the console)
	 * @append true if we append the line to an existing file, false if we destroy it first
	 * @param writer that is opened before calling the function
	 */
	public void printSummaryStats (PrintWriter writer, boolean append) {
		
		String toPrint = "keyNameExp;" + this.expName + ";netWealth;" 
				+ String.format("%.4f", this.avgnetWealth[(this.numberSteps - 1)]) + ";" + 
				String.format("%.4f", this.stdnetWealth[(this.numberSteps - 1)]) + ";TP;" 
				+ String.format("%.4f", this.avgTP[(this.numberSteps - 1)]) + ";" + 
				String.format("%.4f", this.stdTP[(this.numberSteps - 1)]) 	+ ";UP;" + 
				String.format("%.4f", this.avgUP[(this.numberSteps - 1)]) + ";" + 
				String.format("%.4f", this.stdUP[(this.numberSteps - 1)]) + ";TC;" 
				+ String.format("%.4f", this.avgTC[(this.numberSteps - 1)]) + ";" + 
				String.format("%.4f", this.stdTC[(this.numberSteps - 1)]) + ";UC;" 
				+ String.format("%.4f", this.avgUC[(this.numberSteps - 1)]) + ";" + 
				String.format("%.4f", this.stdUC[(this.numberSteps - 1)]) + ";strategyChanges;" 
				+ String.format("%.4f", this.avgStrategyChanges[(this.numberSteps - 1)]) + ";" + 
				String.format("%.4f", this.stdStrategyChanges[(this.numberSteps - 1)]) + ";";
		
		if (append) {
			writer.append (toPrint);
			writer.append("\n");
		} else {
			writer.println (toPrint);
		}
					
			
	
	}
	
	/**
	 * This method prints (by appending to an existing file) the 
	 * summarized stats (avg and std of MC runs) to a stream file (or to the console)
	 * @param writer that is opened before calling the function
	 */
	public void appendSummaryStats (PrintWriter writer) {		
		printSummaryStats (writer, true);				
	}
	
	/**
	 * This method prints all the stats of the MC runs (by appending to an existing file) 
	 * to a stream file (or to the console)
	 * @param writer that is opened before calling the function
	 */
	public void appendAllStats (PrintWriter writer) {		
		printAllStats (writer, true);				
	}

	
	/**
	 * This method prints all the stats of the MC runs to a stream file (or to the console)
	 * @param writer that is opened before calling the function
	 * @append true if we append the line to an existing file, false if we destroy it first
	 */
	public void printAllStats (PrintWriter writer, boolean append) {
		 			
		for (int i = 0; i < this.numberRuns; i++) {
			
			String toPrint = "keyNameExp;" + this.expName + ";MCrun;" + i + ";netWealth;" + 
					String.format("%.4f", this.netWealth[i][(this.numberSteps - 1)]) + ";TP;" 
				+  String.format("%d", this.TP[i][(this.numberSteps - 1)]) 
				+ ";UP;" + String.format("%d", this.UP[i][(this.numberSteps - 1)]) + ";TC;" 
				+ String.format("%d", this.TC[i][(this.numberSteps - 1)]) + ";UC;" 
						+ String.format("%d", this.UC[i][(this.numberSteps - 1)]) + ";strategyChanges;" 
				+ String.format("%d", this.strategyChanges[i][(this.numberSteps - 1)]) + ";\n";
		
			
			if (append) 	
				writer.append (toPrint);				
			else 					
				writer.print (toPrint);					
		}	
	}
	
	/**
	 * This method prints summarized stats of the last quartile of the simulation (avg and std of MC runs) 
	 * to a stream file (or to the console)
	 * @append true if we append the line to an existing file, false if we destroy it first
	 * @param writer that is opened before calling the function
	 */
	public void printSummaryStatsByAveragingLastQuartile (PrintWriter writer, boolean append) {
		
		double lastQuartileNW = 0.;
		double lastQuartileTP = 0.;
		double lastQuartileUP = 0.;
		double lastQuartileTC = 0.;
		double lastQuartileUC = 0.;
		double lastQuartileStrategyChanges = 0.;

		double lastQuartileNWStd = 0.;
		double lastQuartileTPStd = 0.;
		double lastQuartileUPStd = 0.;
		double lastQuartileTCStd = 0.;
		double lastQuartileUCStd = 0.;
		double lastQuartileStrategyChangesStd = 0.;
		
		// check the number of steps which means the last 25% of them
		int quartileSteps = (int) Math.round(0.25*this.numberSteps);
		
		for ( int j = 1; j <= quartileSteps; j++) {
			
			// averaging the MC outputs for the last quartile of the simulation
			lastQuartileNW += this.avgnetWealth[(this.numberSteps - j)];
			lastQuartileTP += this.avgTP[(this.numberSteps - j)];
			lastQuartileUP += this.avgUP[(this.numberSteps - j)];
			lastQuartileTC += this.avgTC[(this.numberSteps - j)];
			lastQuartileUC += this.avgUC[(this.numberSteps - j)];
			lastQuartileStrategyChanges += this.avgStrategyChanges[(this.numberSteps - j)];
			
			// averaging the MC stdev outputs for the last quartile of the simulation
			lastQuartileNWStd += this.stdnetWealth[(this.numberSteps - j)];
			lastQuartileTPStd += this.stdTP[(this.numberSteps - j)];
			lastQuartileUPStd += this.stdUP[(this.numberSteps - j)];
			lastQuartileTCStd += this.stdTC[(this.numberSteps - j)];
			lastQuartileUCStd += this.stdUC[(this.numberSteps - j)];
			lastQuartileStrategyChangesStd += this.stdStrategyChanges[(this.numberSteps - j)];			
			
		}

		lastQuartileNW /= quartileSteps;
		lastQuartileTP /= quartileSteps;
		lastQuartileUP /= quartileSteps;
		lastQuartileTC /= quartileSteps;
		lastQuartileUC /= quartileSteps;
		lastQuartileStrategyChanges /= quartileSteps;
		
		lastQuartileNWStd /= quartileSteps;
		lastQuartileTPStd /= quartileSteps;
		lastQuartileUPStd /= quartileSteps;
		lastQuartileTCStd /= quartileSteps;
		lastQuartileUCStd /= quartileSteps;
		lastQuartileStrategyChangesStd /= quartileSteps;
		
		String toPrint = "LQkeyNameExp;" + this.expName + ";netWealthLQ;" 
				+ String.format("%.4f", lastQuartileNW) + ";" + String.format("%.4f", lastQuartileNWStd) + ";TPLQ;" 
				+ String.format("%.4f", lastQuartileTP) + ";" + String.format("%.4f", lastQuartileTPStd) 
				+ ";UPLQ;" + String.format("%.4f", lastQuartileUP) + ";" + String.format("%.4f", lastQuartileUPStd) + ";TCLQ;" 
				+ String.format("%.4f", lastQuartileTC) + ";" + String.format("%.4f", lastQuartileTCStd) + ";UCLQ;" 
						+ String.format("%.4f", lastQuartileUC) + ";" + String.format("%.4f", lastQuartileUCStd) + ";strategyChangesLQ;" 
				+ String.format("%.4f", lastQuartileStrategyChanges) + ";" + String.format("%.4f", lastQuartileStrategyChangesStd) + ";";
				
		if (append) {
			writer.append (toPrint);
			writer.append("\n");
		} else {
			writer.println (toPrint);
		}	
			
	}
	
	
	/**
	 * This method prints all the averaging stats in the last quartile of the simulation for all 
	 * the MC runs to a stream file (or to the console)
	 * @param writer that is opened before calling the function
	 * @append true if we append the line to an existing file, false if we destroy it first
	 */
	public void printAllStatsByAveragingLastQuartile (PrintWriter writer, boolean append) {
		
		for ( int i = 0; i < this.numberRuns; i++) {
			
			double lastQuartileNW = 0.;
			double lastQuartileTP = 0.;
			double lastQuartileUP = 0.;
			double lastQuartileTC = 0.;
			double lastQuartileUC = 0.;
			double lastQuartileStrategyChanges = 0.;
			
			// check the number of steps which means the last 25% of them
			int quartileSteps = (int)Math.round(0.25*this.numberSteps);
			
			for ( int j = 1; j <= quartileSteps; j++) {
				lastQuartileNW += this.netWealth[i][(this.numberSteps - j)];
				lastQuartileTP += this.TP[i][(this.numberSteps - j)];
				lastQuartileUP += this.UP[i][(this.numberSteps - j)];
				lastQuartileTC += this.TC[i][(this.numberSteps - j)];
				lastQuartileUC += this.UC[i][(this.numberSteps - j)];
				lastQuartileStrategyChanges += this.strategyChanges[i][(this.numberSteps - j)];
			}

			lastQuartileNW /= quartileSteps;
			lastQuartileTP /= quartileSteps;
			lastQuartileUP /= quartileSteps;
			lastQuartileTC /= quartileSteps;
			lastQuartileUC /= quartileSteps;
			lastQuartileStrategyChanges /= quartileSteps;
			
			String toPrint = "LQKeyNameExp;" + this.expName + ";MCrun;" + i + ";netWealthLQ;" + 
					String.format("%.4f", lastQuartileNW) + ";TPLQ;" 
					+  String.format("%.4f", lastQuartileTP) 
					+ ";UPLQ;" + String.format("%.4f", lastQuartileUP) + ";TCLQ;" 
					+ String.format("%.4f", lastQuartileTC) + ";UCLQ;" 
							+ String.format("%.4f", lastQuartileUC) + ";strategyChangesLQ;" 
					+ String.format("%.4f", lastQuartileStrategyChanges) + ";\n";
					
			if (append) 
				writer.append (toPrint);		
			else 
				writer.print (toPrint);					
			
		}	
		
	}
	
	
	/**
	 * This method is for calculating all the statistical information for 
	 * the runs of the metrics
	 * 	 
	 */
	public void calcAllStats () {
					
		for( int j = 0; j < this.numberSteps; j++) {
			
			// Get a DescriptiveStatistics instance
			DescriptiveStatistics statsTP = new DescriptiveStatistics();
			DescriptiveStatistics statsUP = new DescriptiveStatistics();
			DescriptiveStatistics statsTC = new DescriptiveStatistics();
			DescriptiveStatistics statsUC = new DescriptiveStatistics();
			DescriptiveStatistics statsnetWealth = new DescriptiveStatistics();
			DescriptiveStatistics statsStrategyChanges = new DescriptiveStatistics();
			
			// Add the data from the array
			for( int i = 0; i < this.numberRuns; i++) {
				
		        statsTP.addValue(this.TP[i][j]);
		        statsUP.addValue(this.UP[i][j]);
		        statsTC.addValue(this.TC[i][j]);
		        statsUC.addValue(this.UC[i][j]);
		        
		        statsnetWealth.addValue(this.netWealth[i][j]);	  
		        
		        statsStrategyChanges.addValue(this.strategyChanges[i][j]);	 		        
		        
			}

			// calc mean and average for all of them
			
			this.avgTP[j] = statsTP.getMean();	
			this.stdTP[j] = statsTP.getStandardDeviation();
			this.min_TP[j] = statsTP.getMin();	
			this.max_TP[j] = statsTP.getMax();
			
			this.avgUP[j] = statsUP.getMean();	
			this.stdUP[j] = statsUP.getStandardDeviation();
			this.min_UP[j] = statsUP.getMin();	
			this.max_UP[j] = statsUP.getMax();

			this.avgTC[j] = statsTC.getMean();	
			this.stdTC[j] = statsTC.getStandardDeviation();
			this.min_TC[j] = statsTC.getMin();	
			this.max_TC[j] = statsTC.getMax();
			
			this.avgUC[j] = statsUC.getMean();	
			this.stdUC[j] = statsUC.getStandardDeviation();
			this.min_UC[j] = statsUC.getMin();	
			this.max_UC[j] = statsUC.getMax();
			
			this.avgnetWealth[j] = statsnetWealth.getMean();	
			this.stdnetWealth[j] = statsnetWealth.getStandardDeviation();
			this.min_netWealth[j] = statsnetWealth.getMin();	
			this.max_netWealth[j] = statsnetWealth.getMax();

			this.avgStrategyChanges[j] = statsStrategyChanges.getMean();	
			this.stdStrategyChanges[j] = statsStrategyChanges.getStandardDeviation();
			this.min_StrategyChanges[j] = statsStrategyChanges.getMin();	
			this.max_StrategyChanges[j] = statsStrategyChanges.getMax();
		}				
	}

}
