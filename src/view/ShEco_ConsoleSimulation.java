package view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import controller.Controller;
import model.Model;
import model.ModelParameters;

//----------------------------- MAIN FUNCTION -----------------------------//

/**
 * Main function for the simulation software (Sharing Economy Trust model)
 * 
 * @param args
 */

public class ShEco_ConsoleSimulation {		

	// constants to get different options for simple or SA runs
	public final static int NO_SA = 0;
	//public final static int SA_INITIAL_POPULATION = 1;
	public final static int SA_R = 2;
	public final static int SA_R_X = 3;
	
	
	// LOGGING
	private static final Logger log = Logger.getLogger( Model.class.getName() );
	
	/**
	 * Create an options class to store all the arguments of the command-line call of the program
	 * 
	 * @param options the class containing the options, when returned. It has to be created before calling
	 */
	private static void createArguments (Options options) {
				
		options.addOption("paramsFile", true, "Pathfile with the parameters file");
		options.getOption("paramsFile").setRequired(true);
		
		options.addOption("SNFile", true, "File with the SN to run");
		
		options.addOption("outputFile", true, "File to store all the information about the simulation");
		options.getOption("outputFile").setRequired(true);
		
		options.addOption("numTP", true, "number of TP agents at the beginning of the simulation");
		options.addOption("numTC", true, "number of TC agents at the beginning of the simulation");
		options.addOption("numUP", true, "number of UP agents at the beginning of the simulation");
		options.addOption("numUC", true, "number of UC agents at the beginning of the simulation");
		
		options.addOption("Temp", true, "Temp parameter value");
		options.addOption("R", true, "R parameter value");
		options.addOption("S", true, "Loss parameter value for provider when customer is untrustworthy");
		options.addOption("X", true, "X parameter value when provider is untrustworthy");
		
		options.addOption("d_T", true, "deposit value for TC when providers are untrustworthy");
		options.addOption("d_U", true, "deposit value for UC when providers are untrustworthy");
		options.addOption("p", true, "penalty value for UP");
		
		options.addOption("maxSteps", true, "Max number of steps of the simulation");

		options.addOption("MC", true, "Number of MC simulations");
		options.addOption("seed", true, "Seed for running the MC simulations");
				
		// parameters for SA

		options.addOption("SA_R", false, "If running a SA over the ratio R");
		options.addOption("SA_R_X", false, "If running a SA over ratios R & X");
		
		/*options.addOption("SA", false, "If running a SA over the initial population parameters");		
				
		options.addOption("SA_trusters_min", true, "Minimum value for parameter % of trusters (OAT SA)");
		options.addOption("SA_trusters_max", true, "Maximum value for parameter % of trusters (OAT SA)");
		options.addOption("SA_trusters_step", true, "Step value forparameter % of trusters (OAT SA)");

		options.addOption("SA_trustworthies_min", true, "Minimum value for parameter % of trustworthies (OAT SA)");
		options.addOption("SA_trustworthies_max", true, "Maximum value for parameter % of trustworthies (OAT SA)");
		options.addOption("SA_trustworthies_step", true, "Step value forparameter % of trustworthies (OAT SA)");
		*/
		options.addOption("help", false, "Show help information");	
					
		
	}
	
	/**
	 * MAIN CONSOLE-BASED FUNCTION TO RUN A SIMPLE RUN OR A SENSITIVITY ANALYSIS OF THE MODEL PARAMETERS
	 * @param args
	 */
	public static void main (String[] args) {
		
		int SA = NO_SA;
		
		String paramsFile = "";
		String outputFile = "";

		ModelParameters params = null;
		
		// parsing the arguments
		Options options = new Options();
		
		createArguments (options);		

		// create the parser
	    CommandLineParser parser = new DefaultParser();
	    
	    try {
	    	
	        // parse the command line arguments for the given options
	        CommandLine line = parser.parse( options, args );

			// get parameters
			params = new ModelParameters();	
			
	        // retrieve the arguments
	        		    
		    if( line.hasOption( "paramsFile" ) )		    
		    	paramsFile = line.getOptionValue("paramsFile");
		    else 		    	
		    	System.err.println( "A parameters file is needed");

		    if( line.hasOption( "outputFile" ) ) 			    
		    	outputFile = line.getOptionValue("outputFile");

		    // read parameters from file
			params.readParameters(paramsFile);

			// set the outputfile
			params.setOutputFile(outputFile);
			
			
			// once parameters from file are loaded, we modify those read by arguments of command line		
			
		    // load the parameters file and later, override them if there are console arguments for these parameters
			   if( line.hasOption( "numTP" ) ) 			    
			    	params.setNumTP(Integer.parseInt(line.getOptionValue("numTP")));
			   if( line.hasOption( "numUP" ) ) 			    
			    	params.setNumUP(Integer.parseInt(line.getOptionValue("numUP")));
			   if( line.hasOption( "numTC" ) ) 			    
			    	params.setNumTC(Integer.parseInt(line.getOptionValue("numTC")));
			   if( line.hasOption( "numUC" ) ) 			    
			    	params.setNumUC(Integer.parseInt(line.getOptionValue("numUC")));
			 
		    // save file for the SN
		    if( line.hasOption( "SNFile" ) )		    
		    	params.setNetworkFilesPattern(line.getOptionValue("SNFile"));		    	
			  
		    // MC
		    if( line.hasOption( "MC" ) ) 			    
		    	params.setRunsMC(Integer.parseInt(line.getOptionValue("MC")));
		  
		    // seed
		    if( line.hasOption( "seed" ) ) 			    
		    	params.setSeed(Long.parseLong(line.getOptionValue("seed")));

		    // maxSteps
		    if( line.hasOption( "maxSteps" ) ) 			    
		    	params.setMaxSteps(Integer.parseInt(line.getOptionValue("maxSteps")));
		    	
		    // Temp
		    if( line.hasOption( "Temp" ) ){
		    	params.setTemp(Integer.parseInt(line.getOptionValue("Temp")));
		    }		  		    	

		    // R
		    if( line.hasOption( "R" ) ){
		    	params.setR(Integer.parseInt(line.getOptionValue("R")));
		    }		  
		    
		    // S
		    if( line.hasOption( "S" ) ){
		    	params.setS(Integer.parseInt(line.getOptionValue("S")));
		    }
		    
		    // X
		    if( line.hasOption( "X" ) ){
		    	params.setX(Integer.parseInt(line.getOptionValue("X")));
		    }		

		    // d_T
		    if( line.hasOption( "d_T" ) ){
		    	params.setDeposit_T(Double.parseDouble(line.getOptionValue("d_T")));
		    }
		    
		    // d_U
		    if( line.hasOption( "d_U" ) ){
		    	params.setDeposit_U(Double.parseDouble(line.getOptionValue("d_U")));
		    }		  
		    
		    // p
		    if( line.hasOption( "p" ) ){
		    	params.setPenalty(Double.parseDouble(line.getOptionValue("p")));
		    }		    		
		    
		    
		    // help information
		    if( line.hasOption("help") ) {
			    	
			    // automatically generate the help statement
			    HelpFormatter formatter = new HelpFormatter();
			    formatter.printHelp( "Trust for Sharing Economy. 21st Dec 2016. Manuel Chica. UOC/UON", options);			   	
			}		    
		    	
			
		    // if a SA is running
		    /*if( line.hasOption( "SA" ) ) {
		    	
		    	SA = SA_INITIAL_POPULATION;
		    	
		    	// we have to get the rest of the parameters of the SA running (they were changed to integer instead of ratios because of 
		    	// differences between machines when calculating the exact number of agents)
		    	
		    	if (line.hasOption( "SA_trustworthies_min" ))
		    		SensitivityAnalysis.trustworthies_min = Integer.parseInt(line.getOptionValue("SA_trustworthies_min"));
		    	if (line.hasOption( "SA_trustworthies_max" ))
		    		SensitivityAnalysis.trustworthies_max = Integer.parseInt(line.getOptionValue("SA_trustworthies_max"));
		    	if (line.hasOption( "SA_trustworthies_step" ))
		    		SensitivityAnalysis.trustworthies_step = Integer.parseInt(line.getOptionValue("SA_trustworthies_step"));
		    	
		    	if (line.hasOption( "SA_trusters_min" ))
		    		SensitivityAnalysis.trusters_min = Integer.parseInt(line.getOptionValue("SA_trusters_min"));
		    	if (line.hasOption( "SA_trusters_max" ))
		    		SensitivityAnalysis.trusters_max = Integer.parseInt(line.getOptionValue("SA_trusters_max"));
		    	if (line.hasOption( "SA_trusters_step" ))
		    		SensitivityAnalysis.trusters_step = Integer.parseInt(line.getOptionValue("SA_trusters_step"));
		    
		    	
		    } else */

		    if( line.hasOption( "SA_R" ) ) {
		    	
		    	// we have to run a SA on the ratio reward R		    	
		    	SA = SA_R;		    	
		    }	
		    
		    if( line.hasOption( "SA_R_X" ) ) {
		    	
		    	// we have to run a SA on the ratio reward R and X at the same time		    	
		    	SA = SA_R_X;		    	
		    }		    	
	    }
	    
	    catch (ParseException exp ) {
	    	
	        // oops, something went wrong
	        System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );

			log.log(Level.SEVERE, "Parsing failed.  Reason: " + exp.toString(), exp);			
	    }
	    
	    	
        System.out.println("\n****** STARTING THE RUN OF THE TRUST DYNAMICS ABM MODEL ******\n");

        Date date = new Date();
        System.out.println("****** " + date.toString() + "******\n");

        File fileAllMC = new File ("./logs/" + "ShEco_AllMCruns_" + params.getOutputFile() + ".txt");
        File fileSummaryMC = new File ("./logs/" + "ShEco_SummaryMCruns_" +  params.getOutputFile() + ".txt");
        File fileAllMCLQ = new File ("./logs/" + "ShEco_AllMCrunsLQ_" + params.getOutputFile() + ".txt");
        File fileSummaryMCLQ = new File ("./logs/" + "ShEco_SummaryMCrunsLQ_" +  params.getOutputFile() + ".txt");
        File fileTimeSeriesMC = new File ("./logs/" + "ShEco_TimeSeriesMCruns_" +  params.getOutputFile() + ".txt");
       
        // the SA check    	    			    	
	    if (SA == NO_SA) {
	    	
	    	// no SA, simple run
	    	
	    	RunStats stats;
	    	
	    	// print parameters for double-checking
	    	System.out.println("-> Parameters values:");
		    PrintWriter out = new PrintWriter(System.out, true);
	        params.printParameters(out);
	        
	        log.log(Level.FINE, "\n*** Parameters values of this model:\n" + params.export());
	        
	        
			// run the MC simulations
			long time1 = System.currentTimeMillis ();
			
		    Controller controller = new Controller (params, paramsFile);

	 		// running the model with the MC simulations
	 		stats = controller.runModel();		
	 		
	 		stats.setExpName(params.getOutputFile());
	 		
	 		long  time2  = System.currentTimeMillis( );
	 		System.out.println("\n****** " + (double)(time2 - time1)/1000 + "s spent during the simulation");
	 		
	 		stats.calcAllStats();
	         
	 		// print the stats in the screen 		
	 		stats.printSummaryStats(out, false);
	 		stats.printSummaryStatsByAveragingLastQuartile(out, false);  // also the last quartile info
	 		System.out.println();
	 		
	 		// print the stats into a file
	        System.out.println("\n****** Stats also saved into a file ******\n");
	         
	        PrintWriter printWriter;
	         
	 		try {
	 			
	 			// print all the runs info into a file
	 			printWriter = new PrintWriter (fileAllMC);
	 			stats.printAllStats (printWriter, false);
	 	        printWriter.close (); 
	 	        
	 	        // print all the runs info (last quartiles of the sims) into a file
	 			printWriter = new PrintWriter (fileAllMCLQ);
	 			stats.printAllStatsByAveragingLastQuartile (printWriter, false);
	 	        printWriter.close ();  
	 	        
	 	        // print the summarized MC runs into a file
	 	        printWriter = new PrintWriter (fileSummaryMC);
	 			stats.printSummaryStats (printWriter, false);
	 	        printWriter.close ();    
	 	        
	 	        // print the summarized MC runs (last quartiles of the sims) into a file
	 	        printWriter = new PrintWriter (fileSummaryMCLQ);
	 			stats.printSummaryStatsByAveragingLastQuartile(printWriter, false);
	 	        printWriter.close ();    

	 	        // print the time series into a file
	 	        printWriter = new PrintWriter (fileTimeSeriesMC);
	 			stats.printTimeSeriesStats (printWriter);
	 	        printWriter.close ();    
	 	        
	 	        
	 		} catch (FileNotFoundException e) {
	 			
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 			
	 		    log.log( Level.SEVERE, e.toString(), e );
	 		} 
	    	
	    } else {
	    
	    	// SA over the k1/k2 relationship
	    	
	    	/*if (SA == SA_INITIAL_POPULATION) {
	    			    	    
	    		SensitivityAnalysis.runSA_k1_k2_k3 (params, paramsFile, fileAllMC, fileSummaryMC, fileAllMCLQ, fileSummaryMCLQ);
	    		
	    	} else */
	    	if ( SA == SA_R) {
	    		
	    		SensitivityAnalysis.runSA_R (params, paramsFile, fileAllMC, fileSummaryMC, fileAllMCLQ, fileSummaryMCLQ);
	    		
	    	} else if ( SA == SA_R_X) {
		    		
		    	SensitivityAnalysis.runSA_R_X (params, paramsFile, fileAllMC, fileSummaryMC, fileAllMCLQ, fileSummaryMCLQ);
		    		
		    }	
	    
	    }
								
	}
	
	
}
