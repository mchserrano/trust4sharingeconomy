package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import sim.engine.*;
import sim.util.*;

import socialnetwork.*;
import util.BriefFormatter;
import util.Colors;

/**
 * Simulation core, responsible for scheduling agents of the SharingEconomy model
 * 
 * @author mchica
 */
public class Model extends SimState {

	// ########################################################################
	// Variables
	// ########################################################################
	
	static final long serialVersionUID = 1L;
		
	static boolean INCLUDEZERO = true;
	static boolean INCLUDEONE = true;
	
	static boolean SHOW_SN = false; 
	
	// LOGGING
	public static final Logger log = Logger.getLogger( Model.class.getName() );
	
	//public static Logger logger = LoggerFactory.getLogger(Model.class);
	//static String LOGFILENAME = "./config/log4j.properties";
	
	static String CONFIGFILENAME;
	
	// MODEL VARIABLES
	
	ModelParameters params;
	
	//double minPayOff;   // min payoff to be obtained by agents for the update rule
	//double maxPayOff;  // max payoff to be obtained by agents for the update rule
	
	Bag agents;	

	int TP_Agents []; 			// a counter of the TP agents during the simulation
	int UP_Agents []; 			// a counter of the UP agents during the simulation
	int TC_Agents []; 			// a counter of the TP agents during the simulation
	int UC_Agents []; 			// a counter of the UP agents during the simulation

	int strategyChanges [];		// array with the total number of strategy changes during the simulation
	
	double globalPayoffs[];		// array with the sum of all the payoffs of the population at each step
			
	
	// SOCIAL NETWORK
	GraphStreamer socialNetwork;
	
	int MC_RUN = -1;
	
	//--------------------------- Clone method ---------------------------//	
	
		
	//--------------------------- Get/Set methods ---------------------------//	
	//
	
	public static String getConfigFileName() {
		return CONFIGFILENAME;
	}
	
	public static void setConfigFileName(String _configFileName) {
		CONFIGFILENAME = _configFileName;
	}

	public GraphStreamer getSocialNetwork() {	
		return socialNetwork;	
	}	
	
	/**
	 * Gets the payoffs of all the agents.
	 * @return
	 */
	public double[] getGlobalPayoffs() {
		return globalPayoffs;
	}

	/**
	 * Sets the payoffs of all the agents
	 * @return
	 */
	public void setGlobalPayoffs(double[] globalPayoffs) {
		this.globalPayoffs = globalPayoffs;
	}
	
	/**
	 * Gets the bag of agents.
	 * @return
	 */
	public Bag getAgents() {
		return agents;
	}

	/**
	 * Sets the bag of agents
	 * @param _agents
	 */
	public void setAgents(Bag _agents) {
		this.agents = _agents;
	}		
			
	/**
	 * Get the global net-wealth of the population at a given step
	 */
	public double getGlobalPayoffAtStep(int _position) {
		return this.globalPayoffs[_position];
	}
	
	/**
	 * Get the number of TP agents at a given step - time
	 * @return the number of TP agents
	 */
	public int getTP_AgentsAtStep (int _position) {
		return TP_Agents[_position];
	}

	/**
	 * Get the number of TP agents for all the period of time
	 * @return an ArrayList with the evolution of the TP agents
	 */
	public int[] getTP_AgentsArray () {
		return TP_Agents;
	}
	
	/**
	 * Get the number of UP agents at a given step - time
	 * @return the number of UP agents
	 */
	public int getUP_AgentsAtStep (int _position) {
		return UP_Agents[_position];
	}

	/**
	 * Get the number of UP agents for all the period of time
	 * @return an ArrayList with the evolution of the UP agents
	 */
	public int[] getUP_AgentsArray () {
		return UP_Agents;
	}
	
	/**
	 * Get the number of TC agents at a given step - time
	 * @return the number of TC agents
	 */
	public int getTC_AgentsAtStep (int _position) {
		return TC_Agents[_position];
	}

	/**
	 * Get the number of TC agents for all the period of time
	 * @return an ArrayList with the evolution of the TC agents
	 */
	public int[] getTC_AgentsArray () {
		return TC_Agents;
	}
	
	/**
	 * Get the number of UC agents at a given step - time
	 * @return the number of UC agents
	 */
	public int getUC_AgentsAtStep (int _position) {
		return UC_Agents[_position];
	}

	/**
	 * Get the number of UC agents for all the period of time
	 * @return an ArrayList with the evolution of the UC agents
	 */
	public int[] getUC_AgentsArray () {
		return UC_Agents;
	}
	/**
	 * Get the number of changes in the strategy of the agents for all the period of time
	 * @return an ArrayList with the evolution of the strategy changes for all the agents
	 */
	public int[] getStrategyChanges_AgentsArray () {
		return strategyChanges;
	}
	
	
	/**
	 * Get the parameter object with all the input parameters
	 */
	public ModelParameters getParametersObject () {
		return  this.params;
	}

	/**
	 * Set the parameters
	 * @param _params the object to be assigned
	 */
	public void setParametersObject (ModelParameters _params) {
		this.params = _params;
	}
	
			
	// ########################################################################
	// Constructors
	// ########################################################################

	/**
	 * Initializes a new instance of the simulation class. 
	 * @param seedIn - a seed provided for the given simulation.
	 */
	public Model(ModelParameters _params) {
	    
		super(_params.getSeed());	
		
		try {  

	        // This block configure the logger with handler and formatter  
			long millis = System.currentTimeMillis();
			FileHandler fh = new FileHandler("./logs/" + _params.getOutputFile() + "_" + millis + ".log");  
	        log.addHandler(fh);
	        BriefFormatter formatter = new BriefFormatter();  
	        fh.setFormatter(formatter);  
	        
	        log.setLevel(Level.FINE);

	        log.log(Level.FINE, "Log file created at " + millis +" (System Time Millis)\n");  

	    } catch (SecurityException e) {  
	        e.printStackTrace(); 	        
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  

		
		// get parameters
		params = _params;		
		
		// store an IDs array with the agents to be k_1, k_2, and k_3 at the beginning of the simulation
		// This is done according to the parameters of their k_1, k_2, and k_3 distribution
		
		//this.scrambledAgentIds = new ArrayList<Integer> ();
		
		// Initialization of the type of strategy counts
		TP_Agents = new int [params.getMaxSteps()];
		UP_Agents = new int [params.getMaxSteps()];
		TC_Agents = new int [params.getMaxSteps()];
		UC_Agents = new int [params.getMaxSteps()];
		
		strategyChanges = new int [params.getMaxSteps()];

		globalPayoffs = new double [params.getMaxSteps()];
				
		// social network initialization from file
				
		socialNetwork = new GraphStreamer(params.nrAgents, params);
		socialNetwork.setGraph(params);
		
		//minPayOff = maxPayOff = -1.;								
		
	}
		
	//--------------------------- SimState methods --------------------------//

	/**
	 * Sets up the simulation. The first method called when the simulation.
	 */
	public void start() {

		super.start();
		
		this.MC_RUN ++;
		
		if ((params.getNumTP() + params.getNumUP() + params.getNumTC() + params.getNumUC()) 
				!= params.nrAgents) {			
			System.err.println(	"Error with the TP, TC, UP, UC distribution. Check parameters "
					+ "to be equal to the total number of agents \n");
		}

        final int FIRST_SCHEDULE = 0;
        int scheduleCounter = FIRST_SCHEDULE;
        
		// at random, create an array with the IDs unsorted. 
		// In this way we can randomly distribute the agents are going to be TP, UP, TC and UC 
		// this is done here to change every MC simulation without changing the SN
		// IMPORTANT! IDs of the SN nodes and agents must be the same to avoid BUGS!!
		
		// init variables for controlling number of agents and payoffs
		for (int i = 0; i < params.getMaxSteps(); i++) {
			TP_Agents[i] = UP_Agents[i] = TC_Agents[i] = UC_Agents[i] = 0;
			
			strategyChanges[i] = 0;
			
			globalPayoffs[i] = 0.;
		}
				
		//System.out.println("min is " + minPayOff + " and max is " + maxPayOff);
		                         
        // Initialization of the agents
        agents = new Bag();
                 
        for (int i = 0; i < params.getNrAgents(); i++) {
        	
            // get a random value from the scrambled IDs data structure and remove it
             
            /*int randomPos = this.random.nextInt(this.scrambledAgentIds.size());
            int idAgent = this.scrambledAgentIds.get(randomPos);
            this.scrambledAgentIds.remove(randomPos);*/
             
            //System.out.print(idAgent + ";");
             
            int strategy = ModelParameters.UNDEFINED_STRATEGY;
             
            if (i < params.numTP) {
                // we need to add it as TP strategy
                strategy = ModelParameters.TRUSTWORTHY_PROVIDER;
                 
            } else if (i < (params.numTP + params.numUP)) {
                // it is a UP strategy
                strategy = ModelParameters.UNTRUSTWORTHY_PROVIDER;
                 
            } else if (i < (params.numTP + params.numUP + params.numTC)) {
                 
                // it is a TC strategy
                strategy = ModelParameters.TRUSTWORTHY_CUSTOMER;
            } else if (i < (params.numTP + params.numUP + params.numTC + params.numUC)) {
                
               // it is a UC strategy
               strategy = ModelParameters.UNTRUSTWORTHY_CUSTOMER;
           }
           
            // generate the agent, push in the bag, and schedule
            GamerAgent cl = generateAgent (i, strategy);  
             
            // Add agent to the list and schedule
            agents.add(cl);
            
            // Add agent to the schedule
            schedule.scheduleRepeating(Schedule.EPOCH, scheduleCounter, cl);
             
            // increase loop variable to control k1, k2 and k3 proportions
            //i++; 
        }       
         
        // shuffle agents in the Bag and later, reassign the id to the position in the bag        
        agents.shuffle(this.random);
        for (int i = 0; i < agents.size(); i++) {
        	//System.out.print("agent with id " + ((GamerAgent) agents.get(i)).getGamerAgentId());
        	((GamerAgent) agents.get(i)).setGamerAgentId(i);
        	//System.out.println(" changed to id " + ((GamerAgent) agents.get(i)).getGamerAgentId());        	
        }
        
        if (Model.SHOW_SN) {
            // to visualize the social network
            this.testShowSN();
        }
                
        // Add anonymous agent to calculate statistics
        setAnonymousAgentApriori(scheduleCounter);
        scheduleCounter++;    
                           
        setAnonymousAgentAposteriori(scheduleCounter);               
					
	}

    // TESTING PURPOSES!! TO SHOW THE SOCIAL NETWORK			
	private void testShowSN() {
        
			 
		  String stylesheet = "graph { " + "  fill-color: rgb(255,255,255);" 
		    + "  padding: 50px;" + "}" + "node { " + "  size: 12px;" 
		    + "  fill-mode: dyn-plain;" 
		    + "  fill-color: red,green,blue,yellow,orange,pink,purple;" 
		    + "}" + "node .membrane {" + "  size: 25px;" + "}" + "edge {" 
		    + "fill-color: grey;" + "}" + "edge.important {" 
		    + "fill-color: red;" + "}" + "edge.reset {" 
		    + "fill-color: grey;" + "}" ; 
		  
		  
		getSocialNetwork().getGraph().addAttribute("ui.stylesheet", stylesheet);
		
        Colors colorPalette = new Colors(6);
        getSocialNetwork().getGraph().addAttribute("ui.default.title", "MC " + this.MC_RUN + ", seed: " + this.seed());

        for (Node node : getSocialNetwork().getGraph()) {	

			node.setAttribute("size", "medium");
			node.addAttribute("ui.label", node.getId());
        }
			
        // for testing, list TP, UP, TC, UC
        System.out.print("Trusters: ");
        for (int i = 0; i < agents.size(); i++) {
        	if (((GamerAgent) agents.get(i)).getCurrentStrategy() == ModelParameters.TRUSTWORTHY_PROVIDER) {
        		System.out.print(((GamerAgent) agents.get(i)).getGamerAgentId() + ",");
        		getSocialNetwork().setNodeColor(i, 1, colorPalette);
        		
        	}
        }
        System.out.println("");
        System.out.print("Trustworthies: ");
        for (int i = 0; i < agents.size(); i++) {
        	if (((GamerAgent) agents.get(i)).getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_PROVIDER) {
        		System.out.print(((GamerAgent) agents.get(i)).getGamerAgentId() + ",");
        		getSocialNetwork().setNodeColor(i, 2, colorPalette);
        	}
        }
        System.out.println("");
        System.out.print("Untrustworthies: ");
        for (int i = 0; i < agents.size(); i++) {
        	if (((GamerAgent) agents.get(i)).getCurrentStrategy() == ModelParameters.TRUSTWORTHY_CUSTOMER) {
        		System.out.print(((GamerAgent) agents.get(i)).getGamerAgentId() + ",");
        		getSocialNetwork().setNodeColor(i, 3, colorPalette);
        	}
        }
        System.out.println("");
        System.out.print("Untrustworthies: ");
        for (int i = 0; i < agents.size(); i++) {
        	if (((GamerAgent) agents.get(i)).getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_CUSTOMER) {
        		System.out.print(((GamerAgent) agents.get(i)).getGamerAgentId() + ",");
        		getSocialNetwork().setNodeColor(i, 4, colorPalette);
        	}
        }
        System.out.println("");
        

		this.socialNetwork.getGraph().display();
        
	}
	
	// TESTING PURPOSES!! TO SHOW THE SOCIAL NETWORK			
	private void updateSN() {
		
		Colors colorPalette = new Colors(6);
	       
		for (int i = 0; i < agents.size(); i++) {
	    	if (((GamerAgent) agents.get(i)).getCurrentStrategy() == ModelParameters.TRUSTWORTHY_PROVIDER) {
	    		getSocialNetwork().setNodeColor(i, 1, colorPalette);
	    		
	    	}
	    }
	    for (int i = 0; i < agents.size(); i++) {
	    	if (((GamerAgent) agents.get(i)).getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_PROVIDER) {
	    		getSocialNetwork().setNodeColor(i, 2, colorPalette);
	    	}
	    }
	    for (int i = 0; i < agents.size(); i++) {
	    	if (((GamerAgent) agents.get(i)).getCurrentStrategy() == ModelParameters.TRUSTWORTHY_CUSTOMER) {
	    		getSocialNetwork().setNodeColor(i, 3, colorPalette);
	    	}
	    }
	    for (int i = 0; i < agents.size(); i++) {
	    	if (((GamerAgent) agents.get(i)).getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_CUSTOMER) {
	    		getSocialNetwork().setNodeColor(i, 4, colorPalette);
	    	}
	    }
	    		
	}
	

	//-------------------------- Auxiliary methods --------------------------//	

	
	/**
	 * Generates the agent with its initial strategy, id, probability to act and 
	 * its computed activity array (from the latter probability).
	 * This is important as we can save time not to ask at each step if the agent is active or not
	 * 
	 * @param _nodeId is the id of the agent
	 * @param _strategy is the type of strategy the agent is going to follow
	 * @return
	 */
	private GamerAgent generateAgent(int _nodeId, int _strategy) {

		GamerAgent  cl = new GamerAgent (_nodeId, _strategy, this.params.maxSteps);

		/*
		// double probToAct = this.random.nextDouble();		
		// cl.setProbabilityToAct(probToAct);
		
		cl.setProbabilityToAct(1); // Changed. All the agents are always active
		
		// compute its array of activity
		boolean active[] = new boolean[this.params.maxSteps];
		
		for (int i = 0; i < active.length; i++) {
				
			// we check if it is active according to its probability	
			double r = this.random.nextDouble (Model.INCLUDEZERO, Model.INCLUDEONE);
			
			if(r < cl.getProbabilityToAct()) {
				
				// the agent is active to cooperate at this step
								
				active[i] = true;
				
			} else {
				
				// it is not active to cooperate
				active[i] = false;
			}		
		}
		
		// save it to the agent's field
		cl.setActiveArray(active); 
		*/

		/*System.out.println("--> IDAgent " + _nodeId + " with " 
		+ this.socialNetwork.getNeighborsOfNode(_nodeId).size() + " neighbors.");*/
				
					
		return cl;
	}	
	
	
	/**
	 * Adds the anonymous agent to schedule (at the beginning of each step), 
	 * which calculates the statistics.
	 * @param scheduleCounter
	 */
	private void setAnonymousAgentApriori(int scheduleCounter) {
				
		// Add to the schedule at the end of each step
		schedule.scheduleRepeating(Schedule.EPOCH, scheduleCounter, new Steppable()
		{ 
			/**
			 * 
			 */
			private static final long serialVersionUID = -2837885990121299044L;

			public void step(SimState state) {

				// reseting colours when showing the SN
				if (Model.SHOW_SN) {
					
					// reset all the edges colors
				    for(Edge e:getSocialNetwork().getGraph().getEachEdge()) {
				    	e.setAttribute("ui.class", "reset");
				    }
				}
			    			    			    							
				// log.log(Level.FINE, "Step " + schedule.getSteps() + "\n");	
			}			
		});
		
		
	}
	
	/**
	 * Adds the anonymous agent to schedule (at the end of each step), 
	 * which calculates the statistics.
	 * @param scheduleCounter
	 */
	private void setAnonymousAgentAposteriori(int scheduleCounter) {
				
		// Add to the schedule at the end of each step
		schedule.scheduleRepeating(Schedule.EPOCH, scheduleCounter, new Steppable()
		{ 
			
			private static final long serialVersionUID = 3078492735754898981L;

			public void step(SimState state) { 
				
				int currentStep = (int) schedule.getSteps();

				// System.out.println("\n****Step " + currentStep);
						
				TP_Agents[currentStep] = UP_Agents[currentStep] = TC_Agents[currentStep]= UC_Agents[currentStep] = 0;
				strategyChanges[currentStep] = 0;
				
				for (int i = 0; i < params.nrAgents; i++) {
					
					if (((GamerAgent) agents.get(i)).getCurrentStrategy() == 
							ModelParameters.TRUSTWORTHY_PROVIDER)  {
						TP_Agents[currentStep] ++;	
					}
					
					if (((GamerAgent) agents.get(i)).getCurrentStrategy() == 
							ModelParameters.UNTRUSTWORTHY_PROVIDER)  {
						UP_Agents[currentStep] ++;	
					}
					
					if (((GamerAgent) agents.get(i)).getCurrentStrategy() == 
							ModelParameters.TRUSTWORTHY_CUSTOMER)  {
						TC_Agents[currentStep] ++;	
					}
					
					if (((GamerAgent) agents.get(i)).getCurrentStrategy() == 
							ModelParameters.UNTRUSTWORTHY_CUSTOMER)  {
						UC_Agents[currentStep] ++;	
					}
					if (((GamerAgent) agents.get(i)).hasChangedStrategyAtCurrentStep() )  {
						strategyChanges[currentStep] ++;	
					}					
					
				}
																	
				// update the payoffs				

				globalPayoffs[currentStep] = 0.;
				
				// we decide whether to apply a well-mixed population (no SN to calculate payoffs so 
				// they are calculated locally) or locally by the SN structure
				
				/*if (params.typeOfNetwork == GraphStreamer.NetworkType.WELL_MIXED_POP) {
										
					// calculate global payoffs and assign the payoff for all the agents of the pop
					calculatePayoffWithNeighborsGlobally ();
					
				} else {*/
					
					// locally assign payoff
					
					for (int i = 0; i < params.nrAgents; i++) {					
						
						globalPayoffs[currentStep] += 
								((GamerAgent) agents.get(i)).calculatePayoffWithNeighbors(state);
						
						//if (((GamerAgent) agents.get(i)).getPayoff(currentStep) != 0)
						// System.out.println("Cumulative payoff value " + i + " is " + globalPayoffs[currentStep]);
						
					}
				/*}*/
									
				/*System.out.println("Step-" + currentStep + ";wealth;" + globalPayoffs[currentStep] );*/
				
								
				// when showing the social network, we have an update and sleep time to see changes
				if (Model.SHOW_SN) {
					
					updateSN();
				    
				    try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				// show the result of the last step in the console				
				/*if (currentStep == (params.getMaxSteps() - 1))
					log.log (Level.FINE, "Final step;wealth;" + globalPayoffs[currentStep] + 
							";TP;" + TP_Agents[currentStep] + ";UP;" + UP_Agents[currentStep] 
									+ ";TC;" + TC_Agents[currentStep] + ";UC;" + UC_Agents[currentStep] 
								+ ";strategyChanges;" + strategyChanges[currentStep]  + "\n");										
					*/			
			}	
		});
		
	}
		
	//----------------------------- I/O methods -----------------------------//
	
	/**
	 * Prints simple statistics evolution during the time.
	 */
	public void printStatisticsScreen () {
		
		GamerAgent gamerAgent;
		
		int allTP, allUP, allTC, allUC;
		int tmp;
		
		allTP = allUP = allTC = allUC = 0;
		
		for(int i = 0; i < params.nrAgents; i++) {
			
			gamerAgent = (GamerAgent) agents.get(i);
			tmp = gamerAgent.getCurrentStrategy();
			
			if(tmp == ModelParameters.TRUSTWORTHY_PROVIDER)
				allTP++;
			
			if(tmp == ModelParameters.UNTRUSTWORTHY_PROVIDER)
				allUP++;
			
			if(tmp == ModelParameters.TRUSTWORTHY_CUSTOMER)
				allTC++;
			
			if(tmp == ModelParameters.UNTRUSTWORTHY_CUSTOMER)
				allUC++;
					
						
			// obtain avg. degree of the agent
			ArrayList<Integer> neighbors = 	(ArrayList<Integer>) 
					this.socialNetwork.getNeighborsOfNode(gamerAgent.gamerAgentId);
			
			System.out.print("A-" + gamerAgent.getGamerAgentId() + ";" +  
					+ neighbors.size()  + ";");
					
			// in case we have the whole status tracking, we calculate changes and evolution
			if (ModelParameters.STORE_WHOLE_AGENT_STATUS) {
				
				// obtain number of strategy changes during the simulation
				int evolution[] = gamerAgent.getEvolutionStrategies();
				int changes = 0;
				for (int j = 1; j < evolution.length; j++) {
					if (evolution[j] != evolution[j-1]) {
						// there is a change in the strategy
						changes ++;
					}
				}

				System.out.print(changes + ";" +  evolution[0] + ";"  
						+ gamerAgent.getCurrentStrategy() + "\n");				
			}
			
		}
		
		// Print it out
		System.out.println();
		System.out.print("TP;" + allTP);
		System.out.print(";UP;" + allUP);		
		System.out.print(";TC;" + allTC);	
		System.out.print(";UC;" + allUC);
		System.out.println();
		
	}

}
