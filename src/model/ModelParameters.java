package model;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSourceDGS;

import socialnetwork.GraphStreamer.NetworkType;
import util.*;

/**
 * Parameters of the model of evolutionary dynamics for the Sharing Economy
 * 
 * @author mchica
 * @date 2016/12/21
 *
 */

public class ModelParameters {

	// Read configuration file
	ConfigFileReader config;

	// CONFIGURATION TO SAVE THE WHOLE STATES OF EACH AGENT 
	// IMPORTANT: this is not enabled by default because of the high consumption of memory (heap error when having >10.000 agents)
	public final static boolean STORE_WHOLE_AGENT_STATUS = false;
	public final static int LAST_STEPS_TO_STORE_AGENT_STATUS = 5;
	
	// FOR THE STRATEGY TYPE FOR AGENTS
	public final static int UNDEFINED_STRATEGY = 0;
	public final static int TRUSTWORTHY_PROVIDER = 1;
	public final static int UNTRUSTWORTHY_PROVIDER = 2;
	public final static int TRUSTWORTHY_CUSTOMER = 3;
	public final static int UNTRUSTWORTHY_CUSTOMER = 4;

	public final static int WELL_MIXED_POP = -1;
	public final static int SF_NETWORK = 0;
	public final static int ER_NETWORK = 1;
	public final static int SW_NETWORK = 2;
	public final static int PGP_NETWORK = 3;
	public final static int EMAIL_NETWORK = 4;
	public final static int REGULAR_NETWORK = 5;

	// TYPE OF UPDATE RULE FOR THE AGENTS
	public final static int PROPORTIONAL_UPDATE_RULE = 1;
	public final static int UI_UPDATE_RULE = 2;
	public final static int VOTER_UPDATE_RULE = 3;
	public final static int FERMI_UPDATE_RULE = 4;
	public final static int MORAN_UPDATE_RULE = 5;

	// ########################################################################
	// Variables
	// ########################################################################

	String outputFile;

	// modified just to use a static SN from a file

	// FILE_NETWORK ONLY!!
	// Choose between several social networks:
	// SCALE_FREE_NETWORK, scale-free (Barabasi)
	// ER_RANDOM_NETWORK
	// SW_RANDOM_NETWORK
	NetworkType typeOfNetwork;

	String networkFilesPattern;

	// graph read from file
	Graph graphFromFile;

	// parameters for calculating the payoff matrix of the sharing economy game
	// restriction: Temp > R > S > X   AND  2R > Temp
	
	double Temp; 	// this is the temptation to defect for the customer. 
				 	// It is untrustworthy and, for instance, destroy something in the flat etc...
	
	double R; 		// reward value when customer and provider are trustworthy and cooperate
					// and transaction if correctly finished
	
	double S; 		// loss for the provider when the customer is untrustworthy and losses something
					// from the property he is offering
		
	double X; 		// benefit for the provider & lost for the customer when the transaction is initiated
					// but the provider is not trustworthy (the provider gets the initial fee of the customer
					// but there is not service for the customer)

	double penalty; // penalty (% of X) for providers when they are untrustworthy: They get (1-p)X instead of just X (first model)


	double deposit_T; // deposit (% of X) to compensate trustworthy consumers when providers are untrustworthy. 
						// Therefore, they obtain lower losses (d_T-1)X than in the original model (-X)
	
	double deposit_U; // deposit (% of X) to compensate untrustworthy consumers when providers are untrustworthy. 
						// Therefore, they obtain lower losses (d_U-1)X than in the original model (-X)
	
	// end of the payoffs parameters
	
	
	// population distribution
	int nrAgents;

	int numTP; // number of agents that are trustworthy providers at the beginning
	int numUP; // number of agents that are untrustworthy providers at the beginning
	int numTC; // number of agents that are trustworthy customers at the beginning
	int numUC; // number of agents that are untrustworthy customers at the beginning

	// update's rule for the ev dynamics game
	
	int maxSteps; // maximum number of steps for the simulation
	int runsMC; // number of MC runs
	int updateRule; // identifier for the update rule of the agents

	double q_VM = 1;	// q value in [0,1] to either choose the VM or UI. If 1, always VM. 1-q, UI is applied
		
	long seed; // seed to run all the simulations

	// --------------------------- Get/Set methods ---------------------------//
	//
	
	/**
	 * @return the q value for choosing between VM or UI
	 */
	public double getQ_VM() {
		return q_VM;
	}

	/** 
	 * 
	 * @param q_VM THE q value for choosing between VM or UI
	 */
	public void setQ_VM(double q_VM) {
		this.q_VM = q_VM;
	}
			
	/**
	 * @return the seed for running all the simulations
	 */
	public long getSeed() {
		return seed;
	}

	/**
	 * @param the
	 *            seed to run all the simulation
	 */
	public void setSeed(long _seed) {
		this.seed = _seed;
	}

	/**
	 * @return the deposit for trustworthy consumers when providers are untrustworthy
	 */
	public double getDeposit_T() {
		return this.deposit_T;
	}

	/**
	 * @param _deposit the deposit for trustworthy consumers when providers are untrustworthy
	 *            
	 */
	public void setDeposit_T(double _deposit_T) {
		this.deposit_T = _deposit_T;
	}

	/**
	 * @return the deposit for untrustworthy consumers when providers are untrustworthy
	 */
	public double getDeposit_U() {
		return this.deposit_U;
	}

	/**
	 * @param _deposit the deposit for untrustworthy consumers when providers are untrustworthy
	 *            
	 */
	public void setDeposit_U(double _deposit_U) {
		this.deposit_U = _deposit_U;
	}

	/**
	 * @return the penalty for providers when they are untrustworthy
	 */
	public double getPenalty() {
		return this.penalty;
	}

	/**
	 * @param _penalty the penalty for providers when they are untrustworthy
	 */
	public void setPenalty(double _penalty) {
		this.penalty = _penalty;
	}

	/**
	 * @return the max number of steps of the simulation
	 */
	public int getMaxSteps() {
		return maxSteps;
	}

	/**
	 * @param the
	 *            max number of steps of the simulation
	 */
	public void setMaxSteps(int _maxSteps) {
		this.maxSteps = _maxSteps;
	}

	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}
	
	/**
	 * @return the typeOfNetwork
	 */
	public NetworkType getTypeOfNetwork() {
		return typeOfNetwork;
	}

	/**
	 * @param typeOfNetwork
	 *            the typeOfNetwork to set
	 */
	public void setTypeOfNetwork(NetworkType typeOfNetwork) {
		this.typeOfNetwork = typeOfNetwork;
	}

	/**
	 * @return the graph
	 */
	public Graph getGraph() {
		return graphFromFile;
	}

	/**
	 * @param _graph
	 *            to set
	 */
	public void setGraph(Graph _graph) {
		this.graphFromFile = _graph;
	}

	public String getNetworkFilesPattern() {
		return networkFilesPattern;
	}

	public void setNetworkFilesPattern(String networkFilesPattern) {
		this.networkFilesPattern = networkFilesPattern;
	}

	/**
	 * @param _graph
	 *            to set
	 * @throws IOException
	 */
	public void readGraphFromFile(String fileNameGraph) throws IOException {

		FileSourceDGS fileSource = new FileSourceDGS();
		graphFromFile = new SingleGraph("SNFromFile");

		fileSource.addSink(graphFromFile);
		fileSource.readAll(fileNameGraph);

		fileSource.removeSink(graphFromFile);
		
		//this.displayGraph();
		
		/*System.out.println("number of nodes: " + graphFromFile.getNodeCount());

		int distr[] = Toolkit.degreeDistribution(graphFromFile);
		
		for (int k = 0; k < distr.length; k++)
			System.out.println("degree "  + k + "; " + distr[k]);*/
		
	}

	/**
	 * Display the graph
	 * 
	 * @return
	 */
	public void displayGraph (Set<Integer> k1, Set <Integer> k2, Set<Integer> k3) {
	
		for (Node node : graphFromFile) {
			

			node.addAttribute("ui.color",3);
			node.setAttribute("size", "medium");
			//node.setAttribute("ui:fill-color", "blue");
			node.addAttribute("ui.label", node.getId());
			
			if (k1.contains(Integer.getInteger(node.getId()))) {

				node.setAttribute("ui.color", 0); // The node will be green.
				
			} else if (k2.contains(Integer.getInteger(node.getId()))) {

				node.setAttribute("ui.color", 0.5); // The node will be a mix of green and red.
				
			} else if (k3.contains(Integer.getInteger(node.getId()))) {

				node.setAttribute("ui.color", 1); // The node will be red.
			}
			
		}
	
		graphFromFile.display();	
	}
	
	
	/**
	 * Gets the number of agents.
	 * 
	 * @return
	 */
	public int getNrAgents() {
		return nrAgents;
	}

	/**
	 * Sets the number of agents
	 * 
	 * @param nrAgents
	 */
	public void setNrAgents(int nrAgents) {
		if (nrAgents > 0)
			this.nrAgents = nrAgents;
	}

	/**
	 * Gets the type of update rule for the agents.
	 * 
	 * @return
	 */
	public int getUpdateRule() {
		return this.updateRule;
	}

	/**
	 * Sets the update rule.
	 * 
	 * @param updateRule
	 */
	public void setUpdateRule(int _updateRule) {
		this.updateRule = _updateRule;
	}

	/**
	 * Gets Number of initial trustworthy providers 
	 * 
	 * @return
	 */
	public int getNumTP() {
		return this.numTP;
	}

	/**
	 * Sets numTP (number of initial trustworthy providers)
	 * 
	 * @param _numTP
	 */
	public void setNumTP(int _numTP) {
		this.numTP = _numTP;
	}
	
	/**
	 * Gets Number of initial untrustworthy providers 
	 * 
	 * @return
	 */
	public int getNumUP() {
		return this.numUP;
	}

	/**
	 * Sets numUP (number of initial untrustworthy providers)
	 * 
	 * @param _numUP
	 */
	public void setNumUP(int _numUP) {
		this.numUP = _numUP;
	}

	/**
	 * Gets Number of initial trustworthy customers
	 * 
	 * @return
	 */
	public int getNumTC() {
		return this.numTC;
	}

	/**
	 * Sets numTC (number of initial trustworthy customers)
	 * 
	 * @param _numTC
	 */
	public void setNumTC(int _numTC) {
		this.numTP = _numTC;
	}
	
	/**
	 * Gets Number of initial untrustworthy customers 
	 * 
	 * @return
	 */
	public int getNumUC() {
		return this.numUC;
	}

	/**
	 * Sets numUC (number of initial untrustworthy customers)
	 * 
	 * @param _numUC
	 */
	public void setNumUC(int _numUC) {
		this.numUC = _numUC;
	}
	
	/**
	 * @return number of MC runs
	 */
	public int getRunsMC() {
		return runsMC;
	}

	/**
	 * Sets number of MC runs
	 * 
	 * @param _runsMC
	 */
	public void setRunsMC(int _runsMC) {
		this.runsMC = _runsMC;
	}
	
	/**
	 * @return Temp
	 */
	public double getTemp() {
		return this.Temp;
	}

	/**
	 * Sets the temptation to defect parameter for customers
	 * 
	 * @param _Temp
	 */
	public void setTemp(double _Temp) {
		this.Temp = _Temp;
	}
	
	/**
	 * @return S
	 */
	public double getS() {
		return this.S;
	}

	/**
	 * Sets S parameter (loss for provider when customer defects)
	 * 
	 * @param _S
	 */
	public void setS(double _S) {
		this.S = _S;
	}
	
	/**
	 * @return X
	 */
	public double getX() {
		return this.X;
	}

	/**
	 * Sets X parameter (benefit/loss when transaction is not done because of provider is untrustworthy)
	 * 
	 * @param _X
	 */
	public void setX(double _X) {
		this.X = _X;
	}
	
	/**
	 * @return R
	 */
	public double getR() {
		return this.R;
	}

	/**
	 * Sets the reward when both players are trustworthy and make a transaction
	 * 
	 * @param _R
	 */
	public void setR(double _R) {
		this.R = _R;
	}
	
	
	// ########################################################################
	// Constructors
	// ########################################################################

	public ModelParameters() {

	}

	// ########################################################################
	// Export methods
	// ########################################################################

	public String export() {

		String values = "";

		values += exportGeneral();

		values += exportSN();

		return values;
	}

	private String exportSN() {

		String result = "";

		result += "SNFile = " + this.networkFilesPattern + "\n";
		result += "typeOfSN = " + this.typeOfNetwork + "\n";

		return result;

	}

	private String exportGeneral() {

		String result = "";

		result += "MC_runs = " + this.runsMC + "\n";
		result += "seed = " + this.seed + "\n";

		result += "nrAgents = " + this.nrAgents + "\n";

		result += "maxSteps = " + this.maxSteps + "\n";

		result += "numTP = " + this.numTP + "\n";
		result += "numUP = " + this.numUP + "\n";
		result += "numTC = " + this.numTC + "\n";
		result += "numUC = " + this.numUC + "\n";
		
		result += "Temp = " + this.Temp + "\n";
		result += "S = " + this.S + "\n";
		result += "R = " + this.R + "\n";
		result += "X = " + this.X + "\n";
		result += "penalty = " + this.penalty + "\n";
		result += "deposit_T = " + this.deposit_T + "\n";
		result += "deposit_U = " + this.deposit_U + "\n";

		if (this.updateRule == ModelParameters.PROPORTIONAL_UPDATE_RULE)
			result += "updateRule = PROPORTIONAL_UPDATE_RULE\n";
		
		else if (this.updateRule == ModelParameters.UI_UPDATE_RULE)
			result += "updateRule = UI_UPDATE_RULE\n";
		
		else if (this.updateRule == ModelParameters.VOTER_UPDATE_RULE) {
			result += "updateRule = VOTER_UPDATE_RULE\n";
			result += "q_VM = " + this.q_VM + "\n";
		}			
		
		else if (this.updateRule == ModelParameters.FERMI_UPDATE_RULE)
			result += "updateRule = FERMI_UPDATE_RULE\n";
		
		else if (this.updateRule == ModelParameters.MORAN_UPDATE_RULE)
			result += "updateRule = MORAN_UPDATE_RULE\n";
			
		return result;
	}
	
	/**
	 * Reads parameters from the configuration file.
	 */
	public void readParameters(String CONFIGFILENAME) {

		try {

			// Read parameters from the file
			config = new ConfigFileReader(CONFIGFILENAME);

			config.readConfigFile();

			// Get global parameters
			this.maxSteps = config.getParameterInteger("maxSteps");
			this.runsMC = config.getParameterInteger("MCRuns");
			this.seed = config.getParameterInteger("seed");

			this.nrAgents = config.getParameterInteger("nrAgents");

			// initial number of players
			this.numTP = config.getParameterInteger("numTP");
			this.numUP = config.getParameterInteger("numUP");
			this.numTC = config.getParameterInteger("numTC");
			this.numUC = config.getParameterInteger("numUC");
			
			if ((this.numTP +  this.numUP + this.numTC + this.numUC) != this.nrAgents) {
				throw new IOException("Error when defining players' strategies. They do not sum the no. of agents."
						+ "Check params for TP, TC, UP, and UC\n");
			}

			this.updateRule = config.getParameterInteger("updateRule");

			if (this.updateRule == ModelParameters.VOTER_UPDATE_RULE) {
				try {
					this.q_VM = config.getParameterDouble("q_VM");
				} catch (Exception e) {
					
					// if it is not defined, q by default (= 1). Always VM
					this.q_VM = 1.;
				}
					
				
			} else {
				this.q_VM = -1;
			}
			
			this.Temp = config.getParameterDouble("Temp");
			this.S = config.getParameterDouble("S");
			this.X = config.getParameterDouble("X");
			this.R = config.getParameterDouble("R");

			this.penalty = config.getParameterDouble("penalty");
			this.deposit_T = config.getParameterDouble("deposit_T");
			this.deposit_U = config.getParameterDouble("deposit_U");

			// check that  Temp > R > S > X   AND  2R > Temp
			if ( !(this.Temp > this.R) || !(this.R > this.S) || !(this.S > this.X) ) {
				throw new IOException("Error when setting parameters for payoff. 'Temp > R > S > X'  is not satisfied."
						+ "Check params for Temp, R, S, and X\n");
			}
			if ( !(2*this.R > this.Temp) ) {
				throw new IOException("Error when setting parameters for payoff. '2R > Temp' is not satisfied."
						+ "Check params for Temp, R, S, and X\n");
			}

			if ( (this.deposit_U < 0) ||  (this.deposit_U >= 1) ) {
				throw new IOException("Error when setting parameters for payoff. '0 <= deposit_U < 1' is not satisfied."
						+ "Check params for X and deposit_U\n");
			}

			if ( (this.deposit_T < 0) ||  (this.deposit_T >= 1) ) {
				throw new IOException("Error when setting parameters for payoff. '0 <= deposit_T < 1' is not satisfied."
						+ "Check params for X and deposit_T\n");
			}
			
			if ( (this.penalty < 0) ||  (this.penalty >= 1) ) {
				throw new IOException("Error when setting parameters for payoff. '0 <= penalty < 1' is not satisfied."
						+ "Check params for X and penalty\n");
			}

			
			// Always read social network file but this file can be SF, Random,
			// RW or regular

			setNetworkFilesPattern(config.getParameterString("SNFile"));

			this.readGraphFromFile(networkFilesPattern);

			if (config.getParameterInteger("typeOfNetwork") == SW_NETWORK) {

				typeOfNetwork = NetworkType.SW_NETWORK;
			}
			if (config.getParameterInteger("typeOfNetwork") == ER_NETWORK) {

				typeOfNetwork = NetworkType.RANDOM_NETWORK;
			}
			if (config.getParameterInteger("typeOfNetwork") == SF_NETWORK) {

				typeOfNetwork = NetworkType.SCALE_FREE_NETWORK;
			}
			if (config.getParameterInteger("typeOfNetwork") == WELL_MIXED_POP) {

				typeOfNetwork = NetworkType.WELL_MIXED_POP;
			}
			if (config.getParameterInteger("typeOfNetwork") == PGP_NETWORK) {

				typeOfNetwork = NetworkType.PGP_NETWORK;
			}
			if (config.getParameterInteger("typeOfNetwork") == EMAIL_NETWORK) {

				typeOfNetwork = NetworkType.EMAIL_NETWORK;
			}
			if (config.getParameterInteger("typeOfNetwork") == REGULAR_NETWORK) {

				typeOfNetwork = NetworkType.REGULAR_NETWORK;
			}

		} catch (IOException e) {

			System.err.println("Error with SN file when loading parameters for the simulation " + CONFIGFILENAME + "\n"
					+ e.getMessage());
			e.printStackTrace(new PrintWriter(System.err));
		}
	}

	// ----------------------------- I/O methods -----------------------------//

	/**
	 * Prints simple statistics evolution during the time.
	 */
	public void printParameters(PrintWriter writer) {

		// printing general params
		writer.println(this.export());

	}

}
