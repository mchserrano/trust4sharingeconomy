 package model;

import sim.engine.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Class of the players of the trust game 
 * for the sharing economy 
 * 
 * @author mchica
 * @date 2016/12/21
 *
 */

public class GamerAgent implements Steppable {

	// ########################################################################
	// Variables
	// ########################################################################	

	// LOGGING
	// private static final Logger log = Logger.getLogger( Model.class.getName() );
	
	private static final long serialVersionUID = 1L;

	// to print trace for degugging or not
	static boolean SHOW_DEBUG_TRACE = false; 
	
	//--------------------------------- Fixed -------------------------------//
						
	int gamerAgentId;				// A unique agent Id
	
	// REMOVED NOT TO STORE INNECESARY MEMORY (THE AGENT IS ALWAYS ACTIVE TO ACT)
	// double probabilityToAct;		// a probability of the player to act at every step (initialized to U[0,1])
	
	int maxSteps;					// max number of last steps stored in the arrays of the agent 
	
	int currentStep;				// the current step of the simulation
	
	//------------------------------- Dynamic -------------------------------// 
			

	int currentStrategy = ModelParameters.UNDEFINED_STRATEGY;		// current strategy used by the agent
	
	double payoffs[];				// array with the payoffs obtained at each step
	
	int evolutionStrategies[];		// array with the evolution in the strategies of the player (when it uses the update rule)

	// REMOVED NOT TO STORE INNECESARY MEMORY 
	// boolean active[];				// array, calculated from the probabilityToAct to compute when the agent is active to cooperate
	
	
	
	// ########################################################################
	// Constructors
	// ######################################################################## 		
	

	/**
	 * Initializes a new instance of the ClientAgent class.
	 * @param _gameId is the id for the agent
	 * @param _strategy is the strategy used at the beginning of the simulation
	 * @param _prob the probability to act at each step of the simulation
	 * @param _maxSteps the total max steps of the simulation
	 * 
	 */
	public GamerAgent( int _gamerId, 
					   int _strategy,
						int _maxSteps) { 
	    		
		this.gamerAgentId = _gamerId;
		this.currentStrategy = _strategy;
		
		// check if we wanna store the whole simulation track or just the last steps (i.e., 5)
		if (ModelParameters.STORE_WHOLE_AGENT_STATUS) {
			this.maxSteps = _maxSteps;
		} else {
			this.maxSteps = ModelParameters.LAST_STEPS_TO_STORE_AGENT_STATUS;
		}	
		
		this.evolutionStrategies = new int[(maxSteps)];
		this.payoffs = new double[maxSteps];
		
		for (int i = 0; i < maxSteps; i++) {
			this.payoffs[i] = -1;
			this.evolutionStrategies[i] = -1;
		}				

	}	
	
	// ########################################################################	
	// Methods/Functions 	
	// ########################################################################

	//--------------------------- Get/Set methods ---------------------------//
	
	/**
	 * Gets the id of the agent
	 * @return
	 */
	public int getGamerAgentId() {
		return gamerAgentId;
	}

	/**
	 * Sets the id of the agent
	 * @param gamerAgentId
	 */
	public void setGamerAgentId(int _gamerAgentId) {
		this.gamerAgentId = _gamerAgentId;
	}

	/**
	 * Gets the array of activity 
	 * @return active array
	 * @deprecated
	 */
	public boolean[] getActiveArray() {
		return null;
	}

	/**
	 * Checks if the agent is active or not for a given step
	 * @return the step to check the activity
	 * @deprecated
	 */
	public boolean isActive(int _currentStep) {
		return false;
	}
	
	/**
	 * Sets the array of activity of the agent
	 * @param active
	 * @deprecated
	 */
	public void setActiveArray(boolean[] active) {
		// this.active = active;
	}
	
	/**
	 * Gets the current strategy of the agent
	 * @return - the strategy
	 */
	public int getCurrentStrategy() {
		return currentStrategy;
	}

	/**
	 * Sets the strategy status of the agent
	 * @param _strategy - the current strategy to be set
	 */
	public void setCurrentStrategy (int _strategy) {
		this.currentStrategy = _strategy;
	}

	/**
	 * Gets the payoff array	 
	 */
	public double[] getPayoffs() {
		return payoffs;
	}

	/**
	 * Sets the payoff array
	 * @param _payoffs is the array to set
	 */
	public void setPayoffs(double[] _payoffs) {
		this.payoffs = _payoffs;
	}
	
	/**
	 * Gets the payoff value for a given step	 
	 */
	public double getPayoff(int _step) {
		return payoffs[_step];
	}

	/**
	 * Adds a payoff value to a step but without removing the previous value (i.e., adding it to a previous value)
	 * @param _addPayoff is the netwealth value
	 * @param _step is the step for the value
	 */
	public void addPayoff(double _addPayoff, int _step) {
		this.payoffs[_step] += _addPayoff;
	}

	/**
	 * Sets a payoff value to a step
	 * @param _payoff is the netwealth value
	 * @param _step is the step for the value
	 */
	public void setPayoff(double _payoff, int _step) {
		this.payoffs[_step] = _payoff;
	}

	/**
	 * Gets the probability for the agent to act 
	 * @deprecated
	 */
	public double getProbabilityToAct() {
		return -1;
	}
	
	/**
	 * Sets the probability for the agent to act
	 * @param _probabilityToAct is the probability
	 *  @deprecated
	 */
	public void setProbabilityToAct(double _probabilityToAct) {
		//this.probabilityToAct = _probabilityToAct;
	}

	/**
	 * Gets the array with the changing strategies
	 */
	public int[] getEvolutionStrategies() {
		return evolutionStrategies;
	}
	

	/**
	 * Returns true if the agent has changed its strategy this step. False if it has the same
	 */
	public boolean hasChangedStrategyAtCurrentStep () {
		
		if ( ((this.currentStep > 0) && (this.maxSteps > 1)) && (evolutionStrategies [(this.maxSteps - 1)] != evolutionStrategies [(this.maxSteps - 2)]) ) 
			return true;
		else 
			return false;		
	}
	
	/**
	 * Gets the value of the strategy for a current step 
	 */
	public int getEvolutionStrategies(int _step) {
		return evolutionStrategies [_step];
	}
	
	
	/**
	 * Sets the array with the evolution in the strategy changes
	 * @param _evolutionStrategies is the new array
	 */
	public void setEvolutionStrategies(int[] _evolutionStrategies) {
		this.evolutionStrategies = _evolutionStrategies;
	}
	
	//-------------------------- Cooperation methods --------------------------//


	/**
	 * This function is called to calculate the payoffs of the play of the agent
	 * within its local neighbourhood of the SN. 
	 * The payoff is calculated following the formulae of the n-trust game.
	 *  
	 * @param state - a simulation model object (SimState).
	 * @return the payoff of the agent in this step 
	 */
	public double calculatePayoffWithNeighbors (SimState state) {
		
		Model model = (Model) state;	
				
		ArrayList<Integer> neighbors = 	(ArrayList<Integer>) 
				model.socialNetwork.getNeighborsOfNode(this.gamerAgentId);
		
		// Iterate over neighbors
		
		// setting the payoff to the agent according to the utility matrix
		this.payoffs[(this.maxSteps - 1)] = 0.;
		
		// apart from the neighbors, also count the agent itself		
		
		if (SHOW_DEBUG_TRACE) {
			System.out.println("Focal A-" + this.gamerAgentId + 
					" has str " + this.getCurrentStrategy() + " and " +
					neighbors.size() + " neighs");				
		}
		
		
		for (int i = 0; i < neighbors.size(); i++) {

			GamerAgent neighbor = (GamerAgent) 
				(model.getAgents()).get(neighbors.get(i));

				
			// depending on the strategy of the neighbor and the focal agent itself, 
			// we set the payoff for the pairwise comparison			

			switch (this.getCurrentStrategy()) {
			
				case ModelParameters.TRUSTWORTHY_PROVIDER:
					
					if ( (neighbor.getCurrentStrategy() == ModelParameters.TRUSTWORTHY_PROVIDER) 
							|| (neighbor.getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_PROVIDER) ) {

						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is PROVIDER, then 0");									
						}

						// same profile so no payoff at all
						
					} else if (neighbor.getCurrentStrategy() == ModelParameters.TRUSTWORTHY_CUSTOMER) {					
						
						// focal agent is TP and neighbor is TC, so happy transaction!
						this.payoffs[(this.maxSteps - 1)] += model.getParametersObject().getR();

						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is TC then " + 
									model.getParametersObject().getR() + ". Payoff is " + this.payoffs[(this.maxSteps - 1)]);									
						}
							
					} else if  (neighbor.getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_CUSTOMER) {					
						
						// focal agent is TP and neighbor is UC, so focal agent is losing ...!
						this.payoffs[(this.maxSteps - 1)] += (-1)*model.getParametersObject().getS();

						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is UC then " + 
									-model.getParametersObject().getS() + ". Payoff is " + this.payoffs[(this.maxSteps - 1)]);
							
						}
							
					}
					
					break;
									
				case ModelParameters.UNTRUSTWORTHY_PROVIDER:
					
					if ( (neighbor.getCurrentStrategy() == ModelParameters.TRUSTWORTHY_PROVIDER) 
							|| (neighbor.getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_PROVIDER) ) {

						// same profile so no payoff at all
						
						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is PROVIDER, then 0");									
						}
						
						
					} else if ( (neighbor.getCurrentStrategy() == ModelParameters.TRUSTWORTHY_CUSTOMER) 
							|| (neighbor.getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_CUSTOMER) ) {					
						
						// focal agent is UP so, it does not matter if neighbor is UC or TC, 
						// the customer is losing the initial fee and the focal agent 
						// is getting a value of (1 - penalty)X (MODEL IMPROVEMENT) 
						this.payoffs[(this.maxSteps - 1)] += (1 - model.getParametersObject().getPenalty()) *
								model.getParametersObject().getX();		

						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is CUSTOMER then " + 
									((1 - model.getParametersObject().getPenalty()) * model.getParametersObject().getX())  
									+ ". Payoff is " + this.payoffs[(this.maxSteps - 1)]);									
						}
							
					}
					
					break;
												
						
				case ModelParameters.TRUSTWORTHY_CUSTOMER:	
					
					if ( (neighbor.getCurrentStrategy() == ModelParameters.TRUSTWORTHY_CUSTOMER) 
							|| (neighbor.getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_CUSTOMER) ) {


						// same profile so no payoff at all
						
						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is CUSTOMER, then 0");									
						}
						
					} else if ( neighbor.getCurrentStrategy() == ModelParameters.TRUSTWORTHY_PROVIDER)  {
							
						// both of them are trustworthy and oppostive profile so reward for them
						this.payoffs[(this.maxSteps - 1)] += model.getParametersObject().getR();

						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is TP then " + 
									model.getParametersObject().getR() + ". Payoff is " + this.payoffs[(this.maxSteps - 1)]);									
						}
							
					} else if (neighbor.getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_PROVIDER) {					
						
						// the neighbor is an untrustworthy provider so the customer is losing 
						// (deposit_T - 1) * initial fee (MODEL IMPROVEMENT)
						
						this.payoffs[(this.maxSteps - 1)] += (model.getParametersObject().getDeposit_T() - 1) * 
														model.getParametersObject().getX();		
						
						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is UP then " + 
									((model.getParametersObject().getDeposit_T() - 1) * 
									model.getParametersObject().getX()) + ". Payoff is " + this.payoffs[(this.maxSteps - 1)]);									
						}
							
					}
					
					break;	


				case ModelParameters.UNTRUSTWORTHY_CUSTOMER:	
					
					if ( (neighbor.getCurrentStrategy() == ModelParameters.TRUSTWORTHY_CUSTOMER) 
							|| (neighbor.getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_CUSTOMER) ) {

						// same profile so no payoff at all
						
						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is CUSTOMER, then 0");									
						}
						
						
					} else if ( neighbor.getCurrentStrategy() == ModelParameters.TRUSTWORTHY_PROVIDER)  {
							
						// the customer is getting advantage (temptation to defect value) 
						// from the transaction as it is not untrustworthy
						this.payoffs[(this.maxSteps - 1)] += model.getParametersObject().getTemp();
			

						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is TP then " + 
									model.getParametersObject().getTemp() + ". Payoff is " + this.payoffs[(this.maxSteps - 1)]);									
						}
					
					} else if (neighbor.getCurrentStrategy() == ModelParameters.UNTRUSTWORTHY_PROVIDER) {					

						// the neighbor is an untrustworthy provider so the customer is losing 
						// the initial fee - deposit (MODEL IMPROVEMENT)
						
						this.payoffs[(this.maxSteps - 1)] += (model.getParametersObject().getDeposit_U() - 1) 
								* model.getParametersObject().getX(); 

						if (SHOW_DEBUG_TRACE) {
							System.out.println("Neig A-" + neighbor.gamerAgentId + " is UP then " + 
									((model.getParametersObject().getDeposit_U() - 1) * model.getParametersObject().getX()) 
									+ ". Payoff is " + this.payoffs[(this.maxSteps - 1)]);									
						}
							
					}
					
					break;	

				default: System.err.println( "Fatal Error!! There is an agent with "
						+ "an unknown strategy!!\n");
						
					break;			
			}						
				
		}
												

		if (SHOW_DEBUG_TRACE) {
			System.out.println("A-" + this.gamerAgentId + " was " + this.getCurrentStrategy() 
			+ ", payoff: " + this.payoffs[(this.maxSteps - 1)]);			
		}
		
		return this.payoffs[(this.maxSteps - 1)];
	}
			
	
	/**
	 * With this function we update the strategy using the proportional imitation rule
	 * 
	 * @param state - a simulation model object (SimState).
	 * @return true if the agent has changed its strategy w.r.t. its previous strategy
	 */
	private boolean proportionalImitation (SimState state) {

		Model model = (Model) state;
				
		ArrayList<Integer> neighbors = 	(ArrayList<Integer>) 
				model.socialNetwork.getNeighborsOfNode(this.gamerAgentId);
		
		// first check if the agent has neighbors
		
		if (neighbors.size() > 0) {
			
			int j = model.random.nextInt(neighbors.size());
			GamerAgent neighbor = (GamerAgent) (model.getAgents()).get(neighbors.get(j));

			double neighPayOff = 0.;
			double focalAgentPayOff = 0.;
								
			// TODO CHECK!! calculate max and min payoff values to normalize PROP update rule
			// it is multiplied by avg degree and check if they are greater than them
			double minPayOff = -1 * model.getParametersObject().getS() * neighbors.size();		
			double maxPayOff = model.getParametersObject().getTemp() * neighbors.size();

			if (SHOW_DEBUG_TRACE) {
				System.out.println("minpayoff is " + minPayOff + " and max is " + maxPayOff);				
			}
			
			neighPayOff = neighbor.getPayoff(this.maxSteps - 2);
			focalAgentPayOff = this.getPayoff(this.maxSteps - 2);
			
			if (neighPayOff > maxPayOff) {
				neighPayOff = maxPayOff;
			}			
			
			// compare their payoffs and strategies in the previous step
			if ( neighPayOff > focalAgentPayOff ) {
				
				// the neighbor's strategy was better so we try to shift the strategy with a prob.
				
				double prob = (neighPayOff - focalAgentPayOff) / (maxPayOff - minPayOff);
								
				double r = model.random.nextDouble (Model.INCLUDEZERO, Model.INCLUDEONE);
				

				if (SHOW_DEBUG_TRACE) {
					System.out.println("A-" + this.gamerAgentId + " had payoff of " + focalAgentPayOff 
							+ " and neigh (str. " + neighbor.getEvolutionStrategies(currentStep - 1) + " had payoff of "
							+ neighPayOff + ". Prob is "+ prob);					
				}
				
				// Check if the agent adopts the neighbor's strategy
				if(r < prob) {
					
					// change the strategy!
					this.setCurrentStrategy (neighbor.getEvolutionStrategies(maxSteps - 1));
					
					//Model.log.log(Level.FINE, " Strategy changed!! A-" + this.gamerAgentId + " to strategy " +
					//		this.currentStrategy + " by imitating A-" + neighbor.getGamerAgentId() + "\n");

					return true;
				}
			}		
		}
							
		return false;		
	}
	
	
	/**
	 * With this function we update the strategy used by the agent.
	 * We use the defined imitative ev. dynamics rule for calculating the change (if there is a change)
	 * 
	 * @param state - a simulation model object (SimState).
	 * @return true if the agent has changed its strategy w.r.t. its previous strategy
	 */
	public boolean updateRuleForStrategy (SimState state) {
		
		Model model = (Model) state;
				
		// it does not make sense to do it for the first step
		
		try {
			
			if (currentStep > 0) {
			
				switch (model.getParametersObject().getUpdateRule()) {
				
				case ModelParameters.PROPORTIONAL_UPDATE_RULE:
					
					return this.proportionalImitation(state);
					
				case ModelParameters.UI_UPDATE_RULE:
					
					throw new IOException();											
	
				case ModelParameters.VOTER_UPDATE_RULE:
					
					// hybrid VM with UI. VM is used with prob q while UI is used with prob 1-q
					// when q is 1, the VM is always used for the agent
					
					double r = model.random.nextDouble (Model.INCLUDEZERO, Model.INCLUDEONE);
					
					if (r < model.getParametersObject().getQ_VM()) {
						throw new IOException();				
					} else {
						throw new IOException();					
					}
					
				case ModelParameters.FERMI_UPDATE_RULE:
					
					throw new IOException();
					
				case ModelParameters.MORAN_UPDATE_RULE:
					
					throw new IOException();
							
				}
			}
			
		} catch (IOException e) {

			System.err.println("Error in the agent's action: Update rule not defined for the sharing economy\n");
			e.printStackTrace(new PrintWriter(System.err));
		}		
		
		return false;		
			
	}	


	//--------------------------- Steppable method --------------------------//	
	
	/**
	 * Step of the simulation.
	 * @param state - a simulation model object (SimState).
	 */
	
	//@Override	
	public void step(SimState state) {

		Model model = (Model) state;
		
		currentStep = (int) model.schedule.getSteps();
		
		// shifting all the elements in the tracking status one position to the left
		for (int i = 0; i <(this.maxSteps - 1); i++) {                
			this.evolutionStrategies[i] = this.evolutionStrategies[i + 1];
			this.payoffs[i] = this.payoffs[i + 1];			
		}
				
		this.updateRuleForStrategy(state);
				
		// store the strategy of this step in the last position of the array			
		this.evolutionStrategies[(this.maxSteps - 1)] = this.currentStrategy; 		
		
	}

}


